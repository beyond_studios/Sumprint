@extends('layouts.app')
@section('content')
    <div class="animated fadeInRight">
        <div class="mail-box-header">

            <h3>
                Inbox (16)
            </h3>
        </div>
        <div class="mail-box">

            <div class="panel-chat">
                <div class="side wrapInline">
                    <div class="list-group" id="tabchat">
                        @if(empty($users_session->all()))
                            No hay usuarios activos
                        @else
                            @foreach($users_session as $user)
                                <a href='#' data-chat-token="{{ $user->session  }}" data-session='{{ $user->session  }}' class="list-group-item" data-chat-session="{{ $user->id  }}" id="item-chat-{{ $user->id  }}">
                                    <span class="badge item-count {{ $user->count_messages==0?'hidden':'' }}" style="padding-top: 5px; padding-bottom:3px">{{ $user->count_messages  }}</span>
                                    <h5 style="margin: 0">{!! isset($user->nombre_usuario) ?$user->nombre_usuario:'<span class="text-info">No registrado</span>'  !!}</h5>
                                    <span class="small">{{ $user->dni_usuario }}</span>
                                </a>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="content wrapInline">
                    <div id="content-chat">
                    </div>
                </div>
            </div>


        </div>

    </div>

@stop