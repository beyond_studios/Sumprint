@extends('layouts.app')
@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><b>Pagina de Servicios</b></h2>
    </div>
</div>

{{ Form::open(array('url' => 'webadmin/saveServicio', 'method' => 'post', 'files' => true)) }}

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox-title">
        <h4>Servicios</h4>
        {{ Form::text('id', '', array('class' => 'hidden input-sm form-control')) }}
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="">Título :</label>
                        <div class="form-group">
                            {{ Form::text('titulo', '', array('class' => 'input-sm form-control')) }}
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="">Descripción :</label>
                        {{ Form::textarea('descripcion', '', array('rows'=>'3','class' => 'input-sm form-control')) }}
                    </div>
                </div>
            </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="">Sub Título 1 :</label>
                    {{ Form::text('subtitulo1', '', array('class' => 'input-sm form-control')) }}
                </div>
               <div>
                   <div class="form-group">
                       <label class="">Descripción :</label>
                       {{ Form::textarea('descripcion1', '', array('rows'=>'3','class' => 'input-sm form-control')) }}
                   </div>
               </div>
            </div>
            <div class="col-md-6">
                <div>
                    <div class="form-group">
                        <label class="">Sub Título 2 :</label>
                        {{ Form::text('subtitulo2', '', array('class' => 'input-sm form-control')) }}
                    </div>
                </div>
                <div>
                    <div class="form-group">
                        <label class="">Descripción :</label>
                        {{ Form::textarea('descripcion2', '', array('rows'=>'3','class' => 'input-sm form-control')) }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ibox-content ibox-heading">
        <div class="row">
            <div class="col-md-6">
                <label class="">Imagen :</label>
                <div class="form-group">
                    <div class="uploader-main">
                        <label >
                            {{ Form::file('imagen') }}
                            <i class="fa fa-upload fa-fw"></i>
                            <span> Subir Imagen</span>
                        </label>
                    </div>
                    <div class="alert for--update">
                        {{ Html::image('', '',['width'=>'100','data-type'=>'*imagen','class'=>'icon'])  }}
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <label class="">Icono :</label>
                <div class="form-group">
                    <div class="uploader-main">
                        <label >
                            {{ Form::file('icono') }}
                            <i class="fa fa-upload fa-fw"></i>
                            <span> Subir Imagen</span>
                        </label>
                    </div>
                    <div class="alert for--update">
                        {{ Html::image('', '',['width'=>'100', 'data-type'=>'*icono','class'=>'icon'])  }}
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

    <div class="ibox-content ibox-footer text-right">
        <div class="components">
            {{ Form::reset('Limpiar', ['class'=>'for--save btn btn-default']) }}
            <button type="button" class="btn--cancel btn btn-danger for--update"><i class="fa fa-times fa-fw"></i> Cancelar</button>
        </div>
        <div class="components">
            <button type="submit" class="btn btn-warning for--update"><i class="fa fa-save fa-fw"></i> Actualizar</button>
            <button type="submit" class="btn btn-success for--save"><i class="fa fa-save fa-fw"></i> Agregar</button>
        </div>
    </div>
</div>
{{ Form::close() }}

    <div class="panel">
        <div class="panel-heading">
            <h4>Lista de Servicios</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-bordered ">
                <thead>
                    <tr>
                        <th>Titulo</th>
                        <th width="220" class="text-right">#</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach(\App\Models\servicio::all() as $item)
                        <tr>
                            <td>
                                {{ $item->titulo  }}
                                <div class="content-form hidden">
                                    {{ Form::hidden('tipo', 'SV') }}
                                    {{ Form::text('id', $item->id)  }}
                                    {{ Form::text('titulo', $item->titulo)  }}
                                    {{ Form::text('descripcion', $item->descripcion)  }}
                                    {{ Form::text('subtitulo1', $item->subtitulo1)  }}
                                    {{ Form::text('descripcion1', $item->descripcion1)  }}
                                    {{ Form::text('subtitulo2', $item->subtitulo2)  }}
                                    {{ Form::text('descripcion2', $item->descripcion2)  }}
                                    {{ Form::text('*icono', \Illuminate\Support\Facades\URL::to('/images/servicios/icono/'.$item->icono)) }}
                                    {{ Form::text('*imagen', \Illuminate\Support\Facades\URL::to('/images/servicios/imagen/'.$item->imagen)) }}
                                    {{ Form::text('link', $item->link)  }}
                                </div>
                            </td>
                            <td class="text-right">
                                <a href="#" class="btn--edit btn btn-warning btn-sm"><i class="fa fa-edit"></i> Editar</a>
                                <a href="{{route('webadmin.delete')}}" class="btn--delete btn btn-danger btn-sm"><i class="fa fa-trash"></i> Eliminar</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@stop