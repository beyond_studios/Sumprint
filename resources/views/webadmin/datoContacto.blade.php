@extends('layouts.app')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2><b>Pagina de Contacto</b></h2>
	</div>
</div>
{{ Form::open(array('url' => 'webadmin/saveContacto', 'method' => 'post')) }}
<div class="panel" >
    <div class="panel-heading">
        <h4>Datos de Contacto</h4>
    </div>
    <div class="panel-body">
@foreach(App\Models\contacto::all() as $contacto)
    <div class="row">
    	<div class="col-md-2">
    		<label class="input-sm">Teléfono :</label>
    	</div>
    	<div class="col-md-4">
    		{{ Form::hidden('id', $contacto->id) }}
    		{{ Form::text('telefono', $contacto->telefono, array('class' => 'input-sm form-control')) }}
    	</div>
    	<div class="col-md-2">
    		<label class="input-sm">Correo :</label>
    	</div>
    	<div class="col-md-4">
    		{{ Form::text('correo', $contacto->correo, array('class' => 'input-sm form-control')) }}
    	</div>
    </div>
    <div class="row">
    	<div class="col-md-2">
    		<label class="input-sm">Dirección :</label>
    	</div>
    	<div class="col-md-4">
    		{{ Form::text('direccion', $contacto->direccion, array('class' => 'input-sm form-control')) }}
    	</div>
    </div>
@endforeach
	</div>
	<div class="panel-footer text-right">
		{{ Form::reset('Limpiar', ['class'=>'btn btn-default']) }}
		<button type="submit" class="btn btn-success"><i class="fa fa-save fa-fw"></i> Grabar</button>
	</div>
</div>
{{ Form::close() }}
@stop