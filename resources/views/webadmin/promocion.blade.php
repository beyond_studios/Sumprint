@extends('layouts.app')
@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><b>Pagina de Promociones</b></h2>
    </div>
</div>
	{{ Form::open(array('url' => 'webadmin/savePromocion', 'method' => 'post', 'files' => true)) }}
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox-title">
        <h4>Promociones</h4>
        {{ Form::text('id', '', array('class' => 'hidden input-sm form-control')) }}
    </div>
    <div class="ibox float-e-margins">
    <div class="ibox-content">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="">Título :</label>
                    {{ Form::text('titulo', '', array('class' => 'input-sm form-control')) }}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="">Cabecera :</label>
                    {{ Form::text('cabecera', '', array('class' => 'input-sm form-control')) }}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="">Descripción :</label>
                    {{ Form::textarea('descripcion', '', array('class' => 'input-sm form-control')) }}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="">Detalle :</label>
                    {{ Form::textarea('detalle', '', array('class' => 'input-sm form-control', 'size' => '30x3')) }}
                </div>
            </div>
        </div>

    </div>
        <div class="ibox-content ibox-heading">
            <div class="row">
                <div class="col-md-6">
                    <label class="">Imagen :</label>
                    <div class="uploader-main">
                        <label >
                            {{ Form::file('imagen') }}
                            <i class="fa fa-upload fa-fw"></i>
                            <span> Subir Imagen</span>
                        </label>
                    </div>
                    <div class="alert for--update">
                        {{ Html::image('', '',['width'=>'100','data-type'=>'*imagen','class'=>'icon'])  }}
                    </div>
                </div>
            </div>
        </div>
        <div class="ibox-content ibox-footer text-right">
            <div class="components">
                {{ Form::reset('Limpiar', ['class'=>'for--save btn btn-default']) }}
                <button type="button" class="btn--cancel btn btn-danger for--update"><i class="fa fa-times fa-fw"></i> Cancelar</button>
            </div>
            <div class="components">
                <button type="submit" class="btn btn-warning for--update"><i class="fa fa-save fa-fw"></i> Actualizar</button>
                <button type="submit" class="btn btn-success for--save"><i class="fa fa-save fa-fw"></i> Agregar</button>
            </div>
        </div>
    </div>

</div>
{{ Form::close() }}

    <div class="panel">
        <div class="panel-heading">
            <h4>Lista de Promociones</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-bordered ">
                <thead>
                    <tr>
                        <th>Titulo</th>
                        <th width="220" class="text-right">#</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach(\App\Models\promocion::all() as $item)
                        <tr>
                            <td>
                                {{ $item->titulo  }}
                                <div class="content-form hidden">
                                    {{ Form::hidden('tipo', 'PM') }}
                                    {{ Form::text('id', $item->id)  }}
                                    {{ Form::text('titulo', $item->titulo)  }}
                                    {{ Form::text('cabecera', $item->cabecera)  }}
                                    {{ Form::text('descripcion', $item->descripcion)  }}
                                    {{ Form::text('detalle', $item->detalle)  }}
                                    {{ Form::text('*imagen', \Illuminate\Support\Facades\URL::to('/images/promociones/'.$item->imagen))  }}
                                </div>
                            </td>
                            <td class="text-right">
                                <a href="#" class="btn--edit btn btn-warning btn-sm"><i class="fa fa-edit"></i> Editar</a>
                                <a href="{{route('webadmin.delete')}}" class="btn--delete btn btn-danger btn-sm"><i class="fa fa-trash"></i> Eliminar</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop