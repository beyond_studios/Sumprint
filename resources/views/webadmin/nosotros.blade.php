@extends('layouts.app')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><b>Pagina de Nosotros</b></h2>
    </div>
</div>
<div class="panel">
    <div class="panel-heading">
        <h4>Nosotros</h4>
    </div>
    <div class="panel-body">
{{ Form::open(array('url' => 'webadmin/saveNosotros', 'method' => 'post')) }}
@foreach(App\Models\nosotros::all() as $nosotros)
    <div class="row">
    	<div class="col-md-6">
            <div class="form-group">
                <label class="">Quienes somos :</label>
                {{ Form::hidden('id', $nosotros->id) }}
                {{ Form::textarea('quienessomos', $nosotros->quienessomos, array('class' => 'input-sm form-control')) }}
            </div>
    	</div>
    	<div class="col-md-6">
            <div class="form-group">
                <label class="">Misión :</label>
                {{ Form::textarea('mision', $nosotros->mision, array('class' => 'input-sm form-control')) }}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="">Visión :</label>
                {{ Form::textarea('vision', $nosotros->vision, array('class' => 'input-sm form-control')) }}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="">Imagen :</label>
                <div class="uploader-main">
                    <label >
                        {{ Form::file('imagen') }}
                        <i class="fa fa-upload fa-fw"></i>
                        <span> Subir Imagen</span>
                    </label>
                </div>
                <div class="preview-image">
                    {{ Html::image('images/'.$nosotros->imagen)  }}
                </div>
            </div>
        </div>
    </div>
@endforeach
    </div>
	<div class="panel-footer text-right">
		{{ Form::reset('Limpiar', ['class'=>'btn btn-default']) }}
		<button type="submit" class="btn btn-success"><i class="fa fa-save fa-fw"></i> Grabar</button>
	</div>
</div>
{{ Form::close() }}
@stop