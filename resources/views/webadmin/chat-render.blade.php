
<div class="messagesHistory">
    <div class="contentWrapChat">
        @if(empty($messages->all()))
            No hay mensajes
        @else
            @foreach($messages as $message)

                    <div class="messageBox {{ (!$message->user_send)?'text-left':'text-right' }}">
                        <span style="{{ (!$message->user_send)?'margin-left: 2px; background: #f0f98c':'' }}">{{ $message->mensaje  }}</span>
                    </div>
            @endforeach
        @endif
    </div>
    <div class="boxChatScript">
        {{Form::text('bx', '',['class'=>'bxChatController form-control'])}}
    </div>
</div>