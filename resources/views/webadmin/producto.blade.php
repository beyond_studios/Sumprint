@extends('layouts.app')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><b>Pagina de Producto</b></h2>
    </div>
</div>
{{ Form::open(array('url' => 'webadmin/saveProducto', 'method' => 'post', 'files' => true)) }}

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox-title">
        <h4>Productos</h4>
        {{ Form::text('id', '', array('class' => 'hidden input-sm form-control')) }}
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="">Nombre :</label>
                        {{ Form::text('nombre', '', array('class' => 'input-sm form-control')) }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="">Marca :</label>
                        {{ Form::text('marca', '', array('class' => 'input-sm form-control')) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="">Tipo :</label>
                        {{ Form::select('idtipo', App\Models\tipo_producto::orderBy('nombre')->pluck('nombre', 'id'), null, array('class' => 'input-sm form-control')) }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="">Codigo :</label>
                        {{ Form::text('codigo', '', array('class' => 'input-sm form-control')) }}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="">Especificacion :</label>
                        {{ Form::textarea('especificacion', '', array('class' => 'input-sm form-control', 'rows' => 2)) }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="">Descripcion :</label>
                        {{ Form::textarea('descripcion', '', array('class' => 'input-sm form-control')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="ibox-content ibox-heading">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="input-sm">Destacado :</label>
                        {{ Form::checkbox('top', '1') }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="">Archivo Descarga :</label>
                        <div class="uploader-main">
                            <label >
                                {{ Form::file('link_detalle') }}
                                <i class="fa fa-upload fa-fw"></i>
                                <span> Subir Archivo</span>
                            </label>
                        </div>
                        <div class="alert for--update">
                            {{ Html::link('','',['data-type'=>'+link_detalle','class'=>'icon','target'=>'_blank'])  }}
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label class="">Imagen :</label>
                        <div class="uploader-main">
                            <label >
                                {{ Form::file('imagen') }}
                                <i class="fa fa-upload fa-fw"></i>
                                <span> Subir Imagen</span>
                            </label>
                        </div>
                        <div class="alert for--update">
                            {{ Html::image('', '',['width'=>'100','data-type'=>'*imagen','class'=>'icon'])  }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="">Imagen 2 :</label>
                        <div class="uploader-main">
                            <label >
                                {{ Form::file('imagen2') }}
                                <i class="fa fa-upload fa-fw"></i>
                                <span> Subir Imagen</span>
                            </label>
                        </div>
                        <div class="alert for--update">
                            {{ Html::image('', '',['width'=>'100','data-type'=>'*imagen2','class'=>'icon'])  }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="">Imagen 3 :</label>
                        <div class="uploader-main">
                            <label >
                                {{ Form::file('imagen3') }}
                                <i class="fa fa-upload fa-fw"></i>
                                <span> Subir Imagen</span>
                            </label>
                        </div>
                        <div class="alert for--update">
                            {{ Html::image('', '',['width'=>'100','data-type'=>'*imagen3','class'=>'icon'])  }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="">Imagen 4 :</label>
                        <div class="uploader-main">
                            <label >
                                {{ Form::file('imagen4') }}
                                <i class="fa fa-upload fa-fw"></i>
                                <span> Subir Imagen</span>
                            </label>
                        </div>
                        <div class="alert for--update">
                            {{ Html::image('', '',['width'=>'100','data-type'=>'*imagen4','class'=>'icon'])  }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div class="ibox-content ibox-footer text-right">
        <div class="components">
            {{ Form::reset('Limpiar', ['class'=>'for--save btn btn-default']) }}
            <button type="button" class="btn--cancel btn btn-danger for--update"><i class="fa fa-times fa-fw"></i> Cancelar</button>
        </div>
        <div class="components">
            <button type="submit" class="btn btn-warning for--update"><i class="fa fa-save fa-fw"></i> Actualizar</button>
            <button type="submit" class="btn btn-success for--save"><i class="fa fa-save fa-fw"></i> Agregar</button>
        </div>
    </div>
    </div>
</div>
{{ Form::close() }}

    <div class="panel">
        <div class="panel-heading">
            <h4>Lista de Productos</h4>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-bordered ">
                <thead>
                    <tr>
                        <th>Titulo</th>
                        <th width="220" class="text-right">#</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach(\App\Models\producto::all() as $item)
                        <tr>
                            <td>
                                {{ $item->nombre  }}
                                <div class="content-form hidden">
                                    {{ Form::hidden('tipo', 'PR') }}
                                    {{ Form::text('id', $item->id)  }}
                                    {{ Form::text('nombre', $item->nombre)  }}
                                    {{ Form::text('marca', $item->marca)  }}
                                    {{ Form::text('idtipo', $item->idtipo)  }}
                                    {{ Form::text('*imagen', \Illuminate\Support\Facades\URL::to('/images/productos/'.$item->imagen))  }}
                                    {{ Form::text('*imagen2', \Illuminate\Support\Facades\URL::to('/images/productos/'.$item->imagen2))  }}
                                    {{ Form::text('*imagen3', \Illuminate\Support\Facades\URL::to('/images/productos/'.$item->imagen3))  }}
                                    {{ Form::text('*imagen4', \Illuminate\Support\Facades\URL::to('/images/productos/'.$item->imagen4))  }}
                                    {{ Form::text('codigo', $item->nombre)  }}
                                    {{ Form::text('especificacion', $item->especificacion)  }}
                                    {{ Form::text('descripcion', $item->descripcion)  }}
                                    {{ Form::text('+link_detalle', \Illuminate\Support\Facades\URL::to('/images/productos/detalle/'.$item->link_detalle))  }}
                                    {{ Form::text('top', $item->top)  }}
                                </div>
                            </td>
                            <td class="text-right">
                                <a href="#" class="btn--edit btn btn-warning btn-sm"><i class="fa fa-edit"></i> Editar</a>
                                <a href="{{route('webadmin.delete')}}" class="btn--delete btn btn-danger btn-sm"><i class="fa fa-trash"></i> Eliminar</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop