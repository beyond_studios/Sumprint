@extends('layouts.app')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2><b>Pagina de Eco</b></h2>
	</div>
</div>
<div class="panel">
    <div class="panel-heading">
        <h4>Eco</h4>
    </div>
    <div class="panel-body">
{{ Form::open(array('url' => 'webadmin/saveEco', 'method' => 'post')) }}
@foreach(App\Models\eco::all() as $eco)
    <div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="input-sm">Titulo :</label>
				{{ Form::hidden('id', $eco->id) }}
				{{ Form::text('titulo', $eco->titulo, array('class' => 'input-sm form-control')) }}
			</div>
    	</div>
		<div class="col-md-12">
			<label class="">Descripcion :</label>
			{{ Form::textarea('descripcion', $eco->descripcion, array('class' => 'input-sm form-control')) }}
    	</div>
    </div>
@endforeach
    </div>
	<div class="panel-footer text-right">
		{{ Form::reset('Limpiar', ['class'=>'btn btn-default']) }}
		<button type="submit" class="btn btn-success"><i class="fa fa-save fa-fw"></i> Grabar</button>
	</div>
</div>
{{ Form::close() }}
@stop