@extends('layouts.app')

@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><b>Pagina Tipo Producto</b></h2>
    </div>
</div>
{{ Form::open(array('url' => 'webadmin/saveTipoProducto', 'method' => 'post')) }}
<div class="panel">
    <div class="panel-heading">
        <h4>Tipos de Producto</h4>
        {{ Form::text('id', '', array('class' => 'hidden input-sm form-control')) }}
    </div>
    <div class="panel-body">
        <div class="row">
        	<div class="col-md-2">
        		<label class="input-sm">Tipo :</label>
        	</div>
        	<div class="col-md-4">
        		{{ Form::text('nombre', '', array('class' => 'input-sm form-control')) }}
        	</div>
        </div>
    </div>
    <div class="panel-footer text-right">
        <div class="components">
            {{ Form::reset('Limpiar', ['class'=>'for--save btn btn-default']) }}
            <button type="button" class="btn--cancel btn btn-danger for--update"><i class="fa fa-times fa-fw"></i> Cancelar</button>
        </div>
        <div class="components">
            <button type="submit" class="btn btn-warning for--update"><i class="fa fa-save fa-fw"></i> Actualizar</button>
            <button type="submit" class="btn btn-success for--save"><i class="fa fa-save fa-fw"></i> Agregar</button>
        </div>
    </div>
</div>
{{ Form::close() }}

<div class="panel">
    <div class="panel-heading">
        <h4>Lista de Tipos de Producto</h4>
    </div>
    <div class="panel-body">
        <table class="table table-responsive table-bordered ">
            <thead>
                <tr>
                    <th>Titulo</th>
                    <th width="220" class="text-right">#</th>
                </tr>
            </thead>
            <tbody>
                @foreach(\App\Models\tipo_producto::all() as $item)
                    <tr>
                        <td>
                            {{ $item->nombre  }}
                            <div class="content-form hidden">
                                {{ Form::hidden('tipo', 'TP') }}
                                {{ Form::text('id', $item->id)  }}
                                {{ Form::text('nombre', $item->nombre)  }}
                            </div>
                        </td>
                        <td class="text-right">
                            <a href="#" class="btn--edit btn btn-warning btn-sm"><i class="fa fa-edit"></i> Editar</a>
                            <a href="{{route('webadmin.delete')}}" class="btn--delete btn btn-danger btn-sm"><i class="fa fa-trash"></i> Eliminar</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@stop