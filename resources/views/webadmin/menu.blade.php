<?php
Menu::make('menuAdmin', function($menu){
  $menu->add('Home', array('route' => 'webadmin.contenidoMenu'));
  $menu->add('Nosotros', array('route' => 'webadmin.nosotros'));
  $menu->add('Servicios', array('route' => 'webadmin.servicio'));
  $menu->add('Eco', array('route' => 'webadmin.eco'));
  $menu->add('Promociones', array('route' => 'webadmin.promocion'));
  $menu->add('Tipos de Producto', array('route' => 'webadmin.tipoProducto'));
  $menu->add('Productos', array('route' => 'webadmin.producto'));
  $menu->add('Datos de Contacto', array('route' => 'webadmin.datoContacto'));
  $menu->add('Chat', array('route' => 'webadmin.chat'));
});
?>
<div class="nav-side-menu">
  <div class="brand text-center">
    <a href="">{{ Html::image('images/logo.png') }}</a>
  </div>
  <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
  <div class="menu-list">
    {!! Menu::get('menuAdmin')->asUl() !!}
  </div>

  <div style="padding: 12px;">
    <a class='btn btn-gray btn-block' href="{{ route('logout')  }}">
      <i class="fa fa-fw fa-times"></i>Salir</a>
  </div>
</div>