@extends('layouts.app')
@section('content')



    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2><b>Pagina de Inicio</b></h2>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox-title">
                    <h5>Agregar Slider</h5>
                </div>
                <div class="ibox float-e-margins">
                    {{ Form::open(array('url' => 'webadmin/saveCarruselContenidoMenu', 'method' => 'post', 'files' => true)) }}
                    <div class="ibox-content">
                        {{ Form::text('id', '', array('class' => 'hidden input-sm form-control')) }}
                        <div class="form-group">
                            <label  class="input-sm">
                                {{ Form::checkbox('is_top', 1) }}
                                Producto top</label>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="">Título :</label>
                                    {{ Form::text('titulo_carrusel', '', array('class' => 'input-sm form-control')) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="">Cabecera :</label>
                                    {{ Form::text('cabecera_carrusel', '', array('class' => 'input-sm form-control')) }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="">Descripcion :</label>
                                    {{ Form::text('descripcion_carrusel', '', array('class' => 'input-sm form-control')) }}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="">Link :</label>
                                    {{ Form::text('link', '', array('class' => 'input-sm form-control')) }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ibox-content ibox-heading">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="">Imagen :</label>
                                    <div class="uploader-main">
                                        <label >
                                            {{ Form::file('imagen_carrusel', ['width'=>'100','data-type'=>'*imagen_carrusel','id'=>'imagen_carrusel','class'=>'input-file-custom icon']) }}
                                            <i class="fa fa-upload fa-fw"></i>
                                            <span> Subir Imagen</span>
                                        </label>
                                    </div>
                                    <div class="alert for--update">
                                        {{ Html::image('', '',['width'=>'100','data-type'=>'*imagen_carrusel','class'=>'icon'])  }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="">Icono :</label>
                                    <div class="uploader-main">
                                        {{ Form::file('icono_carrusel', ['id'=>'icono_carrusel','class'=>'input-file-custom']) }}
                                        <label for="icono_carrusel">
                                            <i class="fa fa-upload fa-fw"></i>
                                            <span> Subir Imagen</span>
                                        </label>
                                    </div>
                                    <div class="alert for--update">
                                        {{ Html::image('', '',['width'=>'100','data-type'=>'*icono_carrusel','class'=>'icon'])  }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ibox-content ibox-footer text-right">
                        <div class="components">
                            {{ Form::reset('Limpiar', ['class'=>'for--save btn btn-default']) }}
                            <button type="button" class="btn--cancel btn btn-danger for--update"><i class="fa fa-times fa-fw"></i> Cancelar</button>
                        </div>
                        <div class="components">
                            <button type="submit" class="btn btn-warning for--update"><i class="fa fa-save fa-fw"></i> Actualizar</button>
                            <button type="submit" class="btn btn-success for--save"><i class="fa fa-save fa-fw"></i> Agregar</button>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Slider Lista</h5>
                    </div>
                    <div class="ibox-content">
                    <table class="table table-responsive table-striped table-sort">
                        <thead>
                        <tr>
                            <th>Titulo</th>
                            <th width="220" class="text-right">#</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach(\App\Models\carrusel_home::all() as $item)
                                <tr>
                                    <td>
                                        {{ $item->titulo  }}
                                        <div class="content-form hidden">
                                            {{ Form::checkbox('is_top', $item->is_top) }}
                                            {{ Form::hidden('tipo', 'CR') }}
                                            {{ Form::text('id', $item->id)  }}
                                            {{ Form::text('titulo_carrusel', $item->titulo)  }}
                                            {{ Form::text('cabecera_carrusel', $item->cabecera)  }}
                                            {{ Form::text('descripcion_carrusel', $item->descripcion)  }}
                                            {{ Form::text('*imagen_carrusel', \Illuminate\Support\Facades\URL::to('/images/carrusel-home/'.$item->imagen))  }}
                                            {{ Form::text('*icono_carrusel', \Illuminate\Support\Facades\URL::to('/images/carrusel-home/icono/'.$item->icono))  }}
                                            {{ Form::text('link', $item->link) }}
                                        </div>
                                    </td>
                                    <td class="text-right">
                                        <a href="#" class="btn--edit btn btn-warning btn-sm"><i class="fa fa-edit"></i> Editar</a>
                                        <a href="{{route('webadmin.delete')}}" class="btn--delete btn btn-danger btn-sm"><i class="fa fa-trash"></i> Eliminar</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    {{ Form::open(array('url' => 'webadmin/saveContenidoMenu', 'method' => 'post', 'files' => true)) }}
        <div class="row">
            <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Bloque 1</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-5">
                            @foreach(App\Models\bloque1_home::all() as $bloque1)
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="">Título :</label>
                                            {{ Form::hidden('id_bloque1', $bloque1->id) }}
                                            {{ Form::text('titulo_bloque1', $bloque1->titulo, array('class' => 'input-sm form-control')) }}
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="">Cabecera :</label>
                                            {{ Form::text('cabecera_bloque1', $bloque1->cabecera, array('class' => 'input-sm form-control')) }}
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="">Descripcion :</label>
                                            {{ Form::text('descripcion_bloque1', $bloque1->descripcion, array('class' => 'input-sm form-control')) }}
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="">Link :</label>
                                            {{ Form::text('link_bloque1', $bloque1->link, array('class' => 'input-sm form-control')) }}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="col-sm-5">
                            <div class="form-group">
                                <label class="">Imagen :</label>
                                <div class="uploader-main">
                                    <label>
                                        {{ Form::file('imagen_bloque1') }}
                                        <i class="fa fa-upload fa-fw"></i>
                                        <span> Subir Imagen</span>
                                    </label>
                                </div>
                                <div class="preview-image">
                                    {{ Html::image('images/bloque1-home/'.$bloque1->imagen)  }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

                @foreach(App\Models\bloque2_home::all() as $bloque2)
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="ibox-title">
                                <h5>Contenido Izquierda <small>Bloque 1</small></h5>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="">Título :</label>
                                            {{ Form::hidden('id_bloque2', $bloque2->id) }}
                                            {{ Form::text('titulo1_bloque2', $bloque2->titulo1, array('class' => 'input-sm form-control')) }}
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="">Cabecera :</label>
                                            {{ Form::text('cabecera1_bloque2', $bloque2->cabecera1, array('class' => 'input-sm form-control')) }}
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="">Descripcion :</label>
                                            {{ Form::text('descripcion1_bloque2', $bloque2->descripcion1, array('class' => 'input-sm form-control')) }}
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="">Link :</label>
                                            {{ Form::text('link1_bloque2', $bloque2->link1, array('class' => 'input-sm form-control')) }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ibox-content ibox-footer">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="uploader-main">
                                                <label >
                                                    {{ Form::file('imagen1_bloque2') }}
                                                    <i class="fa fa-upload fa-fw"></i>
                                                    <span> Subir Imagen</span>
                                                </label>
                                            </div>
                                            <div class="preview-image">
                                                {{ Html::image('images/bloque2-home/'.$bloque2->imagen1)  }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="ibox-title">
                                <h5>Contenido Derecha <small>Bloque 1</small></h5>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="">Título :</label>
                                            {{ Form::text('titulo2_bloque2', $bloque2->titulo2, array('class' => 'input-sm form-control')) }}
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="">Cabecera :</label>
                                            {{ Form::text('cabecera2_bloque2', $bloque2->cabecera2, array('class' => 'input-sm form-control')) }}
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="">Descripcion :</label>
                                            {{ Form::text('descripcion2_bloque2', $bloque2->descripcion2, array('class' => 'input-sm form-control')) }}
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="">Link :</label>
                                            {{ Form::text('link2_bloque2', $bloque2->link2, array('class' => 'input-sm form-control')) }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ibox-content ibox-footer">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="uploader-main">
                                                <label >
                                                    {{ Form::file('imagen2_bloque2') }}
                                                    <i class="fa fa-upload fa-fw"></i>
                                                    <span> Subir Imagen</span>
                                                </label>
                                            </div>
                                            <div class="preview-image">
                                                {{ Html::image('images/bloque2-home/'.$bloque2->imagen2)  }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

<div class="panel">
    <div class="panel-heading">
        <h4>Bloque 3</h4>
    </div>
    @foreach(App\Models\bloque3_home::all() as $bloque3)
    <div class="panel-body">
    <div class="row">
        <div class="col-md-2">
            <label class="input-sm">Titulo :</label>
        </div>
        <div class="col-md-4">
            {{ Form::text('titulo_bloque3', $bloque3->titulo, array('class' => 'input-sm form-control')) }}
        </div>
        <div class="col-md-2">
            <label class="input-sm">Cabecera :</label>
        </div>
        <div class="col-md-4">
            {{ Form::text('cabecera_bloque3', $bloque3->cabecera, array('class' => 'input-sm form-control')) }}
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <label class="input-sm">Fondo :</label>
        </div>
        <div class="col-md-4">
            {{ Form::hidden('id_bloque3', $bloque3->id) }}
            <div class="uploader-main">
                <label >
                    {{ Form::file('fondo_bloque3') }}
                    <i class="fa fa-upload fa-fw"></i>
                    <span> Subir Imagen</span>
                </label>
            </div>
            <div class="preview-image">
                {{ Html::image('images/bloque3-home/fondo/'.$bloque3->fondo)  }}
            </div>
        </div>
        <div class="col-md-2">
            <label class="input-sm">Icono :</label>
        </div>
        <div class="col-md-4">
            <div class="uploader-main">
                <label >
                    {{ Form::file('icono_bloque3') }}
                    <i class="fa fa-upload fa-fw"></i>
                    <span> Subir Imagen</span>
                </label>
            </div>
            <div class="preview-image">
                {{ Html::image('images/bloque3-home/icono/'.$bloque3->icono)  }}
            </div>
        </div>
    </div>
    </div>
    @endforeach
</div>
<div class="panel">
    <div class="panel-heading">
        <h4>Bloque 4</h4>
    </div>
    @foreach(App\Models\bloque4_home::all() as $bloque4)
        <div class="panel-body">
            <div class="row">
                <div class="col-md-2">
                    <label class="input-sm">Titulo :</label>
                </div>
                <div class="col-md-4">
                    {{ Form::text('titulo_bloque4', $bloque4->titulo, array('class' => 'input-sm form-control')) }}
                </div>
                <div class="col-md-2">
                    <label class="input-sm">Cabecera :</label>
                </div>
                <div class="col-md-4">
                    {{ Form::text('cabecera_bloque4', $bloque4->cabecera, array('class' => 'input-sm form-control')) }}
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label class="input-sm">Link:</label>
                </div>
                <div class="col-md-4">
                    {{ Form::text('link_bloque4', $bloque4->link, array('class' => 'input-sm form-control')) }}
                </div>
            </div>
        </div>
        <div class="ibox-content ibox-heading">
            <div class="row">
                <div class="col-md-3">
                    <label class="input-sm">Imagen :</label>
                    <div class="uploader-main">
                        <label >
                            {{ Form::file('imagen_bloque4') }}
                            <i class="fa fa-upload fa-fw"></i>
                            <span> Subir Imagen</span>
                        </label>
                    </div>
                    <div class="preview-image min">
                        {{ Html::image('images/bloque4-home/imagen/'.$bloque4->imagen)  }}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="form-group col-sm-4">
                            <label class="input-sm">Icono 1:</label>
                            {{ Form::hidden('id_bloque4', $bloque4->id) }}
                            <div class="uploader-main">
                                <label >
                                    {{ Form::file('icono1_bloque4') }}
                                    <i class="fa fa-upload fa-fw"></i>
                                    <span> Subir Imagen</span>
                                </label>
                            </div>
                            <div class="preview-image min">
                                {{ Html::image('images/bloque4-home/iconos/'.$bloque4->icono1)  }}
                            </div>
                        </div>
                        <div class="form-group col-sm-4">
                            <label class="input-sm">Icono 2:</label>
                            <div class="uploader-main">
                                <label >
                                    {{ Form::file('icono2_bloque4') }}
                                    <i class="fa fa-upload fa-fw"></i>
                                    <span> Subir Imagen</span>
                                </label>
                            </div>
                            <div class="preview-image min">
                                {{ Html::image('images/bloque4-home/iconos/'.$bloque4->icono2)  }}
                            </div>
                        </div>
                        <div class="form-group col-sm-4">
                            <label class="input-sm">Icono 3:</label>
                            <div class="uploader-main">
                                <label >
                                    {{ Form::file('icono3_bloque4') }}
                                    <i class="fa fa-upload fa-fw"></i>
                                    <span> Subir Imagen</span>
                                </label>
                            </div>
                            <div class="preview-image min">
                                {{ Html::image('images/bloque4-home/iconos/'.$bloque4->icono3)  }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    <div class="panel-footer text-right">
        {{ Form::reset('Limpiar', ['class'=>'btn btn-default']) }}
        <button type="submit" class="btn btn-success"><i class="fa fa-save fa-fw"></i> Grabar</button>
    </div>
</div>
</div>

{{ Form::close() }}



@stop