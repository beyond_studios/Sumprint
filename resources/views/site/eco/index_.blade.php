@extends('site.layout')

@section('html_class', 'eco')
@section('footer')
@endsection
@section('content')

@foreach(App\Models\eco::all() as $eco)
<section class="full-box">
  <div class="wrap-box">
    <h1>{{ $eco->titulo }}</h1>
    <p>
      {{ $eco->descripcion }}
    </p>
    <div class="nav-box">
      <a href="#" class='button fluid'>Solicitar Servicio</a>
      <div class="back-nav">
        <a href="#" class='button light white left'>Regresar</a>
      </div>
      </div>
  </div>
</section>
@endforeach



@stop
