@extends('site.layout')

@section('footer')
@parent
@endsection

@section('content')

<div class='fixed-group'>
<div class='main-banner roll-init'>
  <ul id='home-slider' class='slider-default'>
  @foreach (App\Models\carrusel_home::all() as $imagen)
    <li>
      @if($imagen->is_top)
      <span class="label-top"></span>
      @endif

      <div class="container">
        <div class="table-slide">
          <div class="img item-slide">
            <div class="preview">
              <?php if(file_exists('images/carrusel-home/'.$imagen->imagen)){ ?>
              {{ Html::image('images/carrusel-home/'.$imagen->imagen) }}
              <?php } ?>
            </div>
          </div>
          <div class="item-slide">
            <div class="caption ">
              <i>              {{ Html::image('images/carrusel-home/icono/'.$imagen->icono) }}
              </i>
              <h3>{{ $imagen->cabecera }}</h3>
              <h2>{{ $imagen->titulo }}</h2>
              <p>
                {{ $imagen->descripcion }}
              </p>
              <a href="{{ $imagen->link  }}" class='button fluid'>Ver producto</a>
            </div>
          </div>
        </div>
      </div>
    </li>
  @endforeach
  </ul>
</div>
</div>
<div class="section-group">

@foreach(App\Models\bloque1_home::all() as $bloque1)
<section class='product-preview main'>
  <div class="container">
    <div class="pp--row">
        <div class="pp-caption">
          <h3>{{ $bloque1->cabecera }}</h3>
          <h2>{{ $bloque1->titulo }}</h2>
          <p>
            {{ $bloque1->descripcion }}
          </p>
          <a href="{{$bloque1->link_bloque1}}">Ver más</a>
        </div>
        <div class="pp-preview">
          <?php if(file_exists('images/bloque1-home/'.$bloque1->imagen)){ ?>
          {{ Html::image('images/bloque1-home/'.$bloque1->imagen) }}
          <?php } ?>
        </div>
    </div>
  </div>
</section>
@endforeach

@foreach(App\Models\bloque2_home::all() as $bloque2)
<section class='product-preview grid'>
  <div class="container">
    <div class="pp--row-table">
      <div class="pp--row">
        <div class="pp--row-inner">
          <div class="pp-preview">
            <?php if(file_exists('images/bloque2-home/'.$bloque2->imagen1)){ ?>
            {{ Html::image('images/bloque2-home/'.$bloque2->imagen1) }}
            <?php } ?>
          </div>
          <div class="pp-caption">
            <h3>{{ $bloque2->cabecera1}}</h3>
            <h2>{{ $bloque2->titulo1}}</h2>
            <p>
              {{ $bloque2->descripcion1}}
            </p>
            <a href="{{$bloque2->link1_bloque2}}">Ver más</a>
          </div>
        </div>
      </div>
      <div class="pp--row right">
        <div class="pp--row-inner">
          <div class="pp-preview">
            <?php if(file_exists('images/bloque2-home/'.$bloque2->imagen1)){ ?>
            {{ Html::image('images/bloque2-home/'.$bloque2->imagen2) }}
            <?php } ?>
          </div>
          <div class="pp-caption">
            <h3>{{ $bloque2->cabecera2 }}</h3>
            <h2>{{ $bloque2->titulo2 }}</h2>
            <p>
              {{ $bloque2->descripcion2 }}
            </p>
            <a href="{{$bloque2->link2_bloque2}}">Ver más</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endforeach
@foreach(App\Models\bloque3_home::all() as $bloque3)
<section class='mid-banner' style='background-image:url(images/bloque3-home/fondo/{{ $bloque3->fondo }})'>
  <div class="container">
    <div class="mb-caption">
        <div class="wrap-mb-caption">
          <?php if(file_exists('images/bloque2-home/'.$bloque2->imagen1)){ ?>
          {{ Html::image('images/bloque3-home/icono/'.$bloque3->icono) }}
          <?php } ?>
          <h3>{{ $bloque3->cabecera }}</h3>
          <h2>{{ $bloque3->titulo }}</h2>
        </div>
    </div>
  </div>
</section>
@endforeach
@foreach(App\Models\bloque4_home::all() as $bloque4)
<section class='product-preview clean'>
  <div class="container">
    <div class="pp--row">
      <div class="pp-caption">
        <div class="prefix--icons">
            <?php if(file_exists('images/bloque4-home/iconos/'.$bloque4->icono1)){ ?>
            <i>{{ Html::image('images/bloque4-home/iconos/'.$bloque4->icono1) }}</i>
            <?php } ?>
            <?php if(file_exists('images/bloque4-home/iconos/'.$bloque4->icono2)){ ?>
            <i>{{ Html::image('images/bloque4-home/iconos/'.$bloque4->icono2) }}</i>
            <?php } ?>
            <?php if(file_exists('images/bloque4-home/iconos/'.$bloque4->icono3)){ ?>
            <i>{{ Html::image('images/bloque4-home/iconos/'.$bloque4->icono3) }}</i>
            <?php } ?>
        </div>
        <h3>{{ $bloque4->cabecera }}</h3>
        <h2>{{ $bloque4->titulo }}</h2>
        <a href="{{ $bloque4->link_bloque4  }}">Ver más</a>
      </div>
      <div class="pp-preview">
        <?php if(file_exists('images/bloque4-home/imagen/'.$bloque4->imagen)){ ?>
        {{ Html::image('images/bloque4-home/imagen/'.$bloque4->imagen) }}
        <?php } ?>
        </div>
      </div>
    </div>
</section>
@endforeach
</div>

@stop
