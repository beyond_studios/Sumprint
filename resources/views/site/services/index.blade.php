@extends('site.layout')
@section('footer')
@parent
@endsection
@section('content')

<section class="main-title-parallax" style='background-image:url(images/par1.jpg)'>
  <div class="caption">
      <h3>Servicios</h3>
      <h2>Acorde a tus necesidades</h2>
  </div>
</section>
<?php  $band = 0; ?>
@foreach(App\Models\servicio::all() as $servicio)
<?php  $band++; ?>
<section class='service-block {{  ($band%2==0)? 'opaque' :'' }}'>
  <div class="container">
    <div class="inner-info">
      <i>{{  Html::image('images/servicios/icono/'.$servicio->icono)}}</i>
        <h2>{{ $servicio->titulo }}</h2>
        <p>
          {!! $servicio->descripcion  !!}
        </p>
    </div>
    @if($servicio->subtitulo1!="" || $servicio->descripcion1!="" || $servicio->subtitulo2!="" || $servicio->descripcion2!="")
    <div class="neu">
        <div class="neu--row">
            <div class="neu--item">
                <h3>{{ $servicio->subtitulo1 }}</h3>
                <div class="description">
                  {!! $servicio->descripcion1 !!}
                </div>
            </div>
            <div class="neu--item">
              <h3>{{ $servicio->subtitulo2 }}</h3>
              <div class="description">
                {!! $servicio->descripcion2 !!}
              </div>
            </div>
        </div>
    </div>
    @endif
    <div class="nav">
      <a href="{{ route('contact')  }}#contacto" class='button fluid'>Quiero este servicio</a>
        @if($servicio->imagen != '')
      <div class="go-products">
        <a href="{{ route('products')  }}" class='button light'>Ir a productos</a>
      </div>
            @endif
    </div>
    @if($servicio->imagen!="")
    <div class='service-image'>{{  Html::image('images/servicios/imagen/'.$servicio->imagen)}}</div>
    @endif

  </div>
</section>
@endforeach
@stop
