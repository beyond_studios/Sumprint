@extends('site.layout')
@section('footer')
@parent
@endsection

@section('media')
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
<script type="text/javascript">
var map;
function initialize(){var mapOptions={zoom:16,center:new google.maps.LatLng(-12.2964195,-76.8367898),scrollwheel:false};
map=new google.maps.Map(document.getElementById('google-map'),mapOptions);
}
google.maps.event.addDomListener(window,'load',initialize);
</script>
@stop
@section('content')

<?php
  $contacto = App\Models\contacto::first();
?>
<section class="full-info">
  <div class="container">
    <div class="main">
      <i>
        {{ Html::image('images/contact.png') }}
      </i>
      <h2>Contáctanos</h2>
      <p>
        Para obtener más información sobre nuestros servicios y/o productos, comunícate a los números de atención al cliente o escríbenos llenando los campos para poder contactarte inmediatamente.
      </p>
    </div>
    <div class="data">
        <small>Atención al cliente:</small>
        <h3>{{ $contacto->telefono }}</h3>
        <small>Correo Electrónico:</small>
        <h3>{{ $contacto->correo }}</h3>
    </div>
  </div>
</section>
<!--
<section class='map'>
    <div id="google-map" class='inner-map'>

    </div>
    <div class="address">
      <b>Dirección:</b> {{ $contacto->direccion }}
    </div>
</section>
-->
<section  id="contacto">

<div class='form-contact'>
  <div class="container">

    <h3>Escríbenos:</h3>
      <ul>
          @foreach($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
      @if(Session::has('message'))
          <div class="alert alert-info">
              {{Session::get('message')}}
          </div>
      @endif
    <div class="form">
      {{ Form::open(['route'=>'contact.form', 'method'=>'POST', 'id'=>'form-contact'])  }}
        <div class="row-form half">
          <div class="input-form">
              <label for="">Nombres completos</label>
              {{Form::text('name','', ['required','class'=>'require'])}}
          </div>
          <div class="input-form">
              <label for="">Correo electrónico</label>
              {{Form::text('email','', ['required','class'=>'require'])}}
          </div>
        </div>
        <div class="row-form half">
          <div class="input-form">
              <label for="">Teléfono o celular de contacto</label>
              {{Form::text('phone','', ['required','class'=>'require'])}}
          </div>
          <div class="input-form">
              <label for="">Empresa (opcional):</label>
              {{Form::text('company','')}}
          </div>
        </div>
        <div class="row-form">
          <div class="input-form">
          <label for="">Servicio:</label>
            <div class="group-check">
              <label for="">
                <input type="checkbox" name="name" value="">
                <span>Outsourcing o Tercerización</span>
              </label>
              <label for="">
                <input type="checkbox" name="name" value="">
                <span>Alquiler de Productos</span>
              </label>
              <label for="">
                <input type="checkbox" name="name" value="">
                <span>Venta de Insumos</span>
              </label>
              <label for="">
                <input type="checkbox" name="name" value="">
                <span>Impresiones Laser</span>
              </label>
              <label for="">
                <input type="checkbox" name="name" value="">
                <span>Reparación</span>
              </label>
              <label for="">
                <input type="checkbox" name="name" value="">
                <span>Reciclaje de deshechos</span>
              </label>
            </div>
          </div>
        </div>
        <div class="row-form">
          <div class="input-form">
            <label for="">Escribe un mensaje:</label>
            <textarea name="message" rows="4"></textarea>
          </div>
        </div>
        <div class="row-form">
            <button type="submit" name="button" class='button fluid'>Enviar Ahora</button>
        </div>
      {{ Form::close()  }}
    </div>
  </div>
</div>
</section>




@stop
