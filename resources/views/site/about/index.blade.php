@extends('site.layout')
@section('footer')
@parent
@endsection

@section('content')

<section class="main-title-parallax" style='background-image:url(images/par1.jpg)'>
  <div class="caption">
      <h3>Nosotros</h3>
      <h2>Conócenos un poco más</h2>
  </div>
</section>

@foreach(App\Models\nosotros::all() as $nosotros)
<section class='about'>
    <div class="container">
        <div class="caption">
          <i>{{ Html::image('images/about_icon.png') }}</i>
          <h2>Quienes Somos</h2>
          <div class="p">
              {!!  $nosotros->quienessomos !!}
          </div>
        </div>
        <!--
        <div class="preview">
          {{ Html::image('images/about.jpg') }}
        </div>
        -->
        <div class="neu" style="margin-top: 80px;">
            <div class="neu--row">
                <div class="neu--item">
                    <h3>Misión</h3>
                    <div class="description">
                      {!! $nosotros->mision  !!}
                    </div>
                </div>
                <div class="neu--item">
                  <h3>Visión</h3>
                  <div class="description">
                    {!! $nosotros->vision  !!}
                  </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endforeach


@stop
