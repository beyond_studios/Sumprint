@extends('site.layout')

@section('footer')
@parent
@endsection
@section('content')

<section class="main-title-parallax left" style='background-image:url(images/par-3.jpg)'>

  <div class="inner-content">
    <div class="container">
      <div class="image">
        {{ Html::image('images/promo-1.png') }}
      </div>
        <div class="wrap-title">
            <div class="caption">
                <h3>Promociones</h3>
                <h1>Productos de segunda mano
        a excelentes precios</h1>
            </div>

      </div>
    </div>
  </div>

</section>

<section class='min-title'>
  <div class="container">
    <h3>Impresoras, toners y más</h3>
    <h2>Aprovecha ahora!</h2>
  </div>
</section>
@foreach(App\Models\promocion::all() as $promocion)
<section class='promo-preview'>
  <div class="container" >
    <div  class='full-bg' style='background-image:url(images/promociones/{{ $promocion->imagen }})'>
      <div class="promo-content">
          <div class="caption">
              <h4>{{ $promocion->cabecera }}</h4>
              <h3>{{ $promocion->titulo }}</h3>
              <p>
                {!! $promocion->descripcion !!}
              </p>
              <p>
                <h5>Detalles:</h5>
                <span>{!! $promocion->detalle  !!} </span>
              </p>
          </div>
          <div class="more">
              <h3>¿Quieres más detalles?</h3>
              <a href="#" class='button'>Consultar por esta promoción</a>
          </div>
      </div>
    </div>
  </div>
</section>
@endforeach
    <section class="promo-banner ">
        <div class="container" >
            <div class="content-render">

                <div class="caption">
                    <h3>¿Quieres una compra directa de promociones?</h3>
                    <h4>Puedes adquirir estas y más ofertas aquí</h4>
                </div>
                <div class="link">
                    <a href="">{{ Html::image('images/r.png')  }}</a>
                </div>
            </div>
        </div>
    </section>


@stop
