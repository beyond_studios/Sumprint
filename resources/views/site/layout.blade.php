<!DOCTYPE html>
<html class="@yield('html_class') ">
  <head>
    <meta charset="utf-8">
    <title>Quality Sumprint Perú - Alquiler impresoras Lima</title>
    <meta name="description"
          content="Empresa peruana dedicada al outsourcing, mantenimiento, alquiler y más de impresoras a nivel nacional.">
    <meta name="keywords" content="Impresoras Perú, Outsourcing de impresoras, Alquiler impresoras Lima, Arrendamiento, Impresión, Tercerización, mantenimiento, impresoras.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" media="screen" title="no title" charset="utf-8">
    <link rel="shortcut icon" type="image/png" href="favicon.png"/>
    {{ Html::style('dist/css/b.min.css') }}
    {{ Html::style('dist/css/c.min.css') }}
    {{ Html::style('dist/css/pace.css') }}
    {{ Html::script('dist/js/pace.js') }}

    <script type="text/javascript">
        clientToken = {{ Session::get('chat_session')  }}
        _token = "{{csrf_token()}}";
        chat = {};
        chat.session = "{{ route('chat.session') }}";
        chat.url = "{{ route('get.chat') }}";
        chat.message = "{{ route('post.message') }}";
        Pace.on("done", function(){
              $("#page").fadeIn(500);
              var app = new Application();
              app.init();
        });
    </script>
  </head>
  <body>
    <div id='page' style="display: none;">

    <div class="header">
        <div class="top-bar">
          <div class="container">
            <div class="wrap-top">
              <div class="wrap-fast">
                <button type="button" href="#" class='chat--prefab'>
                  <i class='fa fa-comment-o'></i>
                  <span>Chat Online
                  </span></button>
                  <a href="#" id='menu-collapse' class='burg'><i class='fa fa-bars'></i></a>
              </div>
              <div class="logo">
                <a href="{{  route('home')}}" class=''>{{ Html::image('images/logo.png') }}</a>
              </div>
              <div class="main-menu">
                <nav>
                  <ul>

                    <li {{Route::current()->getName()  ==  'about' ? 'class=active' : ''}}><a href="{{ route('about') }}"><span>Nosotros</span></a></li>
                    <li {{Route::current()->getName()  ==  'services' ? 'class=active' : ''}}><a href="{{ route('services') }}"><span>Servicios</span></a></li>
                    <li {{(Route::current()->getName()  ==  'products' || Route::current()->getName() == 'product.detail.id') ? 'class=active' : ''}}><a href="{{ route('products') }}"><span>Productos</span></a></li>
                    <li {{Route::current()->getName()  ==  'promo' ? 'class=active' : ''}}><a href="{{ route('promo') }}"><span>Promociones</span></a></li>
                    <li {{Route::current()->getName()  ==  'eco' ? 'class=active' : ''}}><a href="{{ route('eco') }}"><span>Eco</span></a></li>
                    <li {{Route::current()->getName()  ==  'contact' ? 'class=active' : ''}}><a href="{{ route('contact') }}"><span>Contáctanos</span></a></li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
      </div>
    </div>
    <div class="content">
      @yield('content')
    </div>
    @section('footer')
    <footer>
      <div class="container">
        <div class="table-footer">
            <div class="item-table logo">
              <div class="wrap">
                {{ Html::image("images/logo-white.png") }}
              </div>
            </div>
            <div class="item-table default">
              <div class="wrap">
                <h3>Impresoras Top</h3>
                @foreach(\App\Models\producto::where('nombre', 'not like', '')->orderBy('id', 'desc')->limit(3)->get() as $item)
                <a href="{{ route('product.detail.id', [$item->id])  }}">{{ $item->nombre }} <i>{{ Html::image('images/min-arrow.png')  }}</i></a>
                @endforeach
              </div>
            </div>
            <div class="item-table default">
              <div class="wrap">
                <h3>Cotización de productos</h3>
                <p>
                  Si estás interesado en alguno denuestros productos, ingresa al
  siguiente enlace:
                </p>

              </div>
              <a href="{{ route('products')  }}" class="button invert">Ver productos</a>
            </div>
            <div class="item-table default">
              <div class="wrap">
                <h3>Promociones</h3>
                @foreach(\App\Models\promocion::orderBy('id', 'desc')->limit(3)->get() as $item)
                <a href="{{ route('promo') }}">{{ $item->titulo }} <i>{{ Html::image('images/min-arrow.png')  }}</i></a>
                @endforeach
              </div>
            </div>
        </div>
      </div>
    </footer>
      @show
    <div class="bottom">
      <div class="container">
        <div class="table-bottom">
          <div class="left">
            <span>© QUALITY SUMPRINT {{ \Carbon\Carbon::today()->format('Y') }} - Todos los derechos reservados</span>
          </div>
          <div class="right">
            <span><a href="#"><small>Síguenos en </small>{{ Html::image("images/fb.png") }}</a></span>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id='chat-composer' class='chat-nemesis hidden'>
  </div>
    {{ Html::script('https://cdn.socket.io/socket.io-1.4.5.js')  }}
    {{ Html::script('dist/js/b.min.js') }}
    {{ Html::script('dist/js/j.min.js') }}
  @yield('media')

  </body>
</html>
