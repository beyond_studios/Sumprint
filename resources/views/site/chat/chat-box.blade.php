<?php $chat_session =  \Illuminate\Support\Facades\Session::get('chat_session') ?>
<div class="nemesis nemesis-content">
    <div class="dismiss-chat hide" id="dismiss-chat-cops">
        <div class="overlay"></div>
        <div class="content-dismiss">
            <div class="wrap-content-dismiss">
                <div class="inner-content-dismiss">
                    <h4>Salir del Chat</h4>
                    <p>Estás a punto de cerrar la ventana
                        del chat online, ¿estás seguro?</p>
                    <a href="#" id='real-close-chat' class="btn btn-close">Cerrar</a>
                    <a href="#" id='nope-close-chat' class="btn btn-cancel">Cancelar</a>

                </div>
            </div>
        </div>
    </div>
  <div class="nemesis-chat-bar"  id="minimized-chat">

        <div class="nemesis-bar-actions" >
            <a href="#"  class="min"></a>
            <a href="#" class="close" id="dismiss-chat"></a>
        </div>
        <span>Chat Online</span><span class="label-status">
         @if(!$chat_session)
          (desconectado)
         @else
          (conectado)
          @endif
      </span>
  </div>
  <div class="nemesis-body">
        <div class="chat-start">
            @if(!$chat_session)
                <div class="nemesis-wrap-body">

                <i>{{ Html::image('images/chat-icon.png') }}</i>
                <h4>Bienvenido al Chat</h4>
                <p>
                  Si tienes alguna consulta o duda
                    escríbenos al chat online para atenderte
                </p>
                <small>Llena tus datos para empezar</small>
                <div class="form-chat-start">
                  {{ Form::open(['id'=>'init-chat']) }}
                    <div class="input-form">
                      {{ Form::text('chat[name]','',['placeholder'=>'Escribe tus nombres']) }}
                    </div>
                    <div class="input-form">
                      {{ Form::text('chat[dni]','',['placeholder'=>'Escribe tu DNI']) }}
                    </div>
                    <div class="input-form">
                      {{ Form::text('chat[phone]','',['placeholder'=>'Escribe tu teléfono de contacto']) }}
                    </div>
                    <div class="input-form">
                      <button type="submit" disabled id='start-chat' name="button">Empezar chat</button>
                    </div>
                  {{ Form::close() }}
                </div>
                </div>

            @else

                <div class="form-chat-composer" id="composer--chat">
                    <div class="broadcast">
                        <i>{{ Html::image('images/chat-icon.png') }}</i>
                        <h4>¡Hola!</h4>
                        <p>
                            Escribe tu consulta
                        </p>
                    </div>
                    @foreach($messages as $message)
                        <div class="{{!$message->user_send?'my':''}} message"><div class="wrap-message">{{$message->mensaje}}</div></div>
                    @endforeach
                </div>

            @endif
        </div>

  </div>
    @if($chat_session)
        <div class="box-send">
            {{ Form::open(['id'=>'open-chat']) }}

            <div class="table__chat">
                <div class="ips--c">
                    <div style="display: none;">
                        {{ Form::text('id_chat_session', $session, ['id'=>'id_chat_session'])  }}
                        {{ Form::text('my_message', '1')  }}
                    </div>

                    {{ Form::text('send', '', ['class'=>'message_chat_box', 'autocomplete'=>'off'])  }}
                </div>
                <div class="ips--s">
                    <button type="submit"><i class="fa fa-fw fa-arrow-right"></i></button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    @endif
</div>
