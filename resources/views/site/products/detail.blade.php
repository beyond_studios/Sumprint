@extends('site.layout')
@section('footer')
    @parent
@endsection

@section('content')

@if($id!="")
<?php $producto = App\Models\producto::find($id); ?>
    <section class="banner-overlay untop">
        <div class="over">
            {{ Html::image('images/prod-2.png') }}
        </div>
    </section>
    <section class='detail-product'>
        <div class="container">
            <div class="detailProducto">
                <div class="col-galeryProduct">
                    <div class="col-img">
                        {{ Html::image('images/productos/'.$producto->imagen) }}
                    </div>
                    <div class="col-thumbs">
                        <h4>Mas fotos</h4>
                            <div class="item-product-thumb">
                                <div class="wrap-item-product">
                                    <div class="preview">
                                        <?php if(file_exists('images/productos/th_'.$producto->imagen)){ ?>
                                        <a href="">
                                            <i class="fa fa-search"></i>
                                            <?php if(file_exists('images/productos/th_'.$producto->imagen)) ?>
                                            {{ Html::image('images/productos/th_'.$producto->imagen) }}</a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="item-product-thumb">
                                <div class="wrap-item-product">
                                    <div class="preview">
                                        <?php 
                                        if(file_exists('images/productos/th_'.$producto->imagen2)){ ?>
                                        <a href="">
                                            <i class="fa fa-search"></i>
                                            {{ Html::image('images/productos/th_'.$producto->imagen2) }}</a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="item-product-thumb">
                                <div class="wrap-item-product">
                                    <div class="preview">
                                        <?php 
                                        if(file_exists('images/productos/th_'.$producto->imagen3)){ ?>
                                        <a href="">
                                            <i class="fa fa-search"></i>
                                            <?php if(file_exists('images/productos/th_'.$producto->imagen3)) ?>
                                            {{ Html::image('images/productos/th_'.$producto->imagen3) }}</a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="item-product-thumb">
                                <div class="wrap-item-product">
                                    <div class="preview">
                                        <?php if(file_exists('images/productos/th_'.$producto->imagen4)){ ?>
                                        <a href="">
                                            <i class="fa fa-search"></i>
                                            <?php if(file_exists('images/productos/th_'.$producto->imagen4)) ?>
                                            {{ Html::image('images/productos/th_'.$producto->imagen4) }}</a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="col-detailProduct">
                    <a href="{{ route('products') }}" class="button left blue back">Volver</a>
                    <?php $tipo = App\Models\tipo_producto::find($producto->idtipo) ?>
                    <h3>{{ $tipo->nombre }}</h3>
                    <h1>{{ $producto->nombre }}</h1>
                    <span class="labelcod">{{ $producto->codigo }}</span>
                    <p>{!! $producto->descripcion !!}</p>

                    <h4>Especificaciones</h4>
                    <div class="span--desc">
                        {!! $producto->especificacion  !!}
                    </div>
                    <div class="botones">
                        <div class="boton">
                            <p>¿Estás interesado?</p>
                            <a href="{{ route('contact') }}" class="button fluid2">Consulta por este producto</a>  
                        </div>
                        <div class="boton">
                            <p>Ficha Ténica{{ $producto->link }}</p>
                            <a href="../images/productos/detalle/{{ $producto->link_detalle }}" class="button download" target="_blank">Descargar</a>        
                        </div>
                    </div>
                    
                </div>

            </div>
        </div>
    </section>
    <section class='related-products'>
        <div class="container">
            <h2>Productos Similares</h2>
            <div class="items min">
                @foreach(App\Models\producto::where(['idtipo'=> $producto->idtipo])->where('id','!=',$producto->id)->limit(4)->get() as $producto2)
                <div class="item-product">
                    <div class="wrap-item-product">

                        @if($producto->top)
                        <i class='mark'>
                        </i>
                        @endif
                        <a href="{{ route('product.detail.id', [$producto->id])  }}">
                            <div class='wrap'>
                          <span>
                            <small>{{ App\Models\tipo_producto::find($producto2->idtipo)->nombre }}</small>
                            <b>{{ $producto2->nombre }}</b>
                            <i class='go'>Ver detalles</i>
                          </span>
                            </div>
                        </a>
                        <div class="preview">
                            {{ Html::image('images/productos/'.$producto2->imagen) }}
                        </div>
                        <h3>{{ App\Models\tipo_producto::find($producto2->idtipo)->nombre }}</h3>
                        <h2>{{ $producto2->nombre }}</h2>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
   </section>
@endif
@stop
