@extends('site.layout')
@section('footer')
@parent
@endsection

@section('content')

<section class="banner-overlay" style='background-image:url(images/par1.jpg)'>
  <div class="over">
    {{ Html::image('images/prod-2.png') }}
  </div>
</section>
<section class='min-title center'>
  <div class="container">
    <h3>Productos</h3>
    <h2>Impresoras e insumos de última generación</h2>
  </div>
</section>

<section class=products-searcher>
  <div class="container">
    <div class="searcher">
      <div class="capability">
        <div class="capability-inner">
          <div class="cp-box-table">
            <div class="cp-box-append cp-cell">
              <ul class="cp-list-append">
              </ul>
            </div>
            <div class="cp-box-input cp-cell">
              {{ Form::text('search-product', '', ['id'=>'search-product','set-placeholder'=>'Buscar productos aquí']) }}
            </div>
          </div>
        </div>
        <div class="content-search">
          <ul class="pool-search">
          @foreach(App\Models\tipo_producto::all() as $tipo)
            <li><a href="#" data-value="{{ $tipo->id  }}">{{ $tipo->nombre }}</a></li>
          @endforeach
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>

<section class='list-products'>
  <div class="container">
    <div class="items" id="p-elements">

      @foreach($productos as $producto)
        @if(file_exists('images/productos/'.$producto->imagen))
          <div class="item-product" data-tipo="{{ $producto->idtipo  }}">
            <div class="wrap-item-product">
              @if($producto->top)
              <i class='mark'>
                <span class="mark-label">Este es un producto top</span>
              </i>
              @endif
              <a href="{{ route('product.detail.id', [$producto->id])  }}">
                <div class='wrap'>
                    <span>
                      <small>{{ App\Models\tipo_producto::find($producto->idtipo)->nombre }}</small>
                      <b>{{ $producto->nombre }}</b>
                      <i class='go'>Ver detalles</i>
                    </span>
                </div>
              </a>
              <div class="preview">
                {{ Html::image('images/productos/'.$producto->imagen) }}
              </div>
              <h3>{{ App\Models\tipo_producto::find($producto->idtipo)->nombre }}</h3>
              <h2>{{ $producto->nombre }}</h2>
              <span href="{{ route('product.detail.id', [$producto->id]) }}" class='button'>Ver detalles</span>
            </div>
          </div>
        @endif
        @endforeach

    </div>
    @if($productos->render())
    <div class="paginator">
      <span class="label">Páginas:</span>
      {{ $productos->render()  }}
    </div>
      @endif
  </div>
</section>
@stop
