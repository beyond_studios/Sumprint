<!DOCTYPE html>
<html lang="en" class="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Sumprint</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        {{ Html::script('https://cdn.socket.io/socket.io-1.4.5.js')  }}
    {{ Html::style('dist/css/webadmin/c.min.css')  }}


    <script type="text/javascript">
        _token = "{{csrf_token()}}";
        chat_partial ="{{ route('webadmin.chat.render')  }}";
        chat_message ="{{ route('webadmin.post.message')  }}";
        chat_users ="{{ route('webadmin.chat.users')  }}";
    </script>
    {{ Html::script('bower_components/jquery/dist/jquery.min.js')  }}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>


    {{ Html::script('dist/js/webadmin/b.min.js')  }}
    {{ Html::script('dist/js/webadmin/b.min.js')  }}
    {{ Html::script('dist/js/webadmin/j.min.js')  }}


    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea',menubar: false, height:100 });</script>
</head>
<body id="app-layout" class="skin2">
        <div class="sidebar">
            @include('webadmin/menu')
        </div>
        <div class="main-content">
            @yield('content')
        </div>

    <!-- JavaScripts -->


        {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>
