@extends('layouts.blank')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <div class="account-wall text-center">
                    <div style="margin-bottom: 30px">
                        {{ Html::image('images/logo.png') }}</div>

                        <div>

                            {!! Form::open(array('url' => '/webadmin/login', 'method'=>'POST', 'class'=>'form form-signin', 'novalidate')) !!}

                            <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
                                <input id="email" type="email" class="form-control" name="username" value="{{ old('username') }}">
                                @if ($errors->has('username'))
                                    <span class="help-block">
                                                <strong>{{ $errors->first('username') }}</strong>
                                            </span>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                <input id="password" type="password" class="form-control" name="password">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-lg btn-block btn-primary">
                                   Ingresar
                                </button>
                            </div>
                            {{ Form::close()  }}
                        </div>

                    </div>
            </div>
        </div>
    </div>
@endsection
