var express = require('express');
var app = express();
var server = app.listen(8367, '0.0.0.0');
var io = require('socket.io').listen(server);


io.on('connection', function(socket) {

    socket.on('request_clients_active',function(response){
        var gs = [];
        for (var socketId in io.sockets.connected) {
            gs.push(socketId);
        }
        socket.emit('response_clients_active', gs);
    });

    socket.on('register_session_first',function(response){
        response.socketId = socket.id;
        socket.emit('response_session_first', response);
    });

    socket.on('update_session',function(response) {
        var gs = [];
        for (var socketId in io.sockets.connected) {
            gs.push(socketId);
        }
        io.sockets.emit('update_session_to_server', gs);
    });

    socket.on('send_message',function(response){

        response.id = socket.id;
        io.sockets.emit('send_message_by_client', response);

    });
    socket.on('send_message_by_server',function(response){
        io.to(response.idClient).emit('send_message_by_server_to_client', response);

    });

    socket.on('register_session',function(response){
        socket.emit('response_register_session', {socketId:socket.id, sessionId : response.id_chat_session});
    });


});

