var gulp = require('gulp'),
	uglify = require('gulp-uglify'),
	sass = require('gulp-sass'),
	concat = require('gulp-concat'),
	watch = require('gulp-watch');


var PATHS_FRONT = {
	SRC : {
		JS : 'public/src/js/*.js',
		CSS : 'public/src/scss/*.scss',
		BOWER_JS : [
			'public/bower_components/jquery/dist/jquery.min.js',
			'public/bower_components/is_js/is.min.js',
			'public/bower_components/bxslider-4/dist/vendor/jquery.easing.1.3.js',
			'public/bower_components/bxslider-4/dist/jquery.bxSlider.js'

		],
		BOWER_CSS : [

		]
	},
	DIST : {
		JS : 'public/dist/js/',
		CSS : 'public/dist/css/'
	}
};
var PATHS_BACK= {
	SRC : {
		JS : [
			'public/src/js/webadmin/Chat.js',
			'public/src/js/webadmin/upload.js',
			'public/src/js/webadmin/UploadFiles.js',
			'public/src/js/webadmin/Admin.js'
		],
		CSS : 'public/src/scss/webadmin/*.scss',
		BOWER_JS : [
			'public/bower_components/jquery/dist/jquery.min.js',
			'public/bower_components/is_js/is.min.js',
			'public/bower_components/bxslider-4/dist/vendor/jquery.easing.1.3.js',
			'public/bower_components/bxslider-4/dist/jquery.bxSlider.js',
			'public/bower_components/UploaderJS/uploader.min.js'
		],
		BOWER_CSS : [

		]
	},
	DIST : {
		JS : 'public/dist/js/webadmin/',
		CSS : 'public/dist/css/webadmin/'
	}
};

gulp.task('scripts', function(){
	return  gulp.src(PATHS_FRONT.SRC.JS)
		.pipe(uglify({outSourceMaps: false}))
		.pipe(concat('j.min.js'))
		.pipe(gulp.dest(PATHS_FRONT.DIST.JS));
});
gulp.task('bower_css', function(){
	return  gulp.src(PATHS_FRONT.SRC.BOWER_CSS)
		.pipe(sass({'outputStyle' : 'compressed'}))
		.pipe(concat('b.min.css'))
		.pipe(gulp.dest(PATHS_FRONT.DIST.CSS));
});
gulp.task('bower_js', function(){
	return  gulp.src(PATHS_FRONT.SRC.BOWER_JS)
		.pipe(uglify({outSourceMaps: false}))
		.pipe(concat('b.min.js'))
		.pipe(gulp.dest(PATHS_FRONT.DIST.JS));
});
gulp.task('styles', function(){
	return  gulp.src(PATHS_FRONT.SRC.CSS)
		.pipe(sass({'outputStyle' : 'compressed'}))
		.pipe(concat('c.min.css'))
		.pipe(gulp.dest(PATHS_FRONT.DIST.CSS));
});


//BACK
gulp.task('scripts_back', function(){
	return  gulp.src(PATHS_BACK.SRC.JS)
		.pipe(uglify({outSourceMaps: false}))
		.pipe(concat('j.min.js'))
		.pipe(gulp.dest(PATHS_BACK.DIST.JS));
});
gulp.task('bower_css_back', function(){
	return  gulp.src(PATHS_BACK.SRC.BOWER_CSS)
		.pipe(sass({'outputStyle' : 'compressed'}))
		.pipe(concat('b.min.css'))
		.pipe(gulp.dest(PATHS_BACK.DIST.CSS));
});
gulp.task('bower_js_back', function(){
	return  gulp.src(PATHS_BACK.SRC.BOWER_JS)
		.pipe(uglify({outSourceMaps: false}))
		.pipe(concat('b.min.js'))
		.pipe(gulp.dest(PATHS_BACK.DIST.JS));
});
gulp.task('styles_back', function(){
	return  gulp.src(PATHS_BACK.SRC.CSS)
		.pipe(sass({'outputStyle' : 'compressed'}))
		.pipe(concat('c.min.css'))
		.pipe(gulp.dest(PATHS_BACK.DIST.CSS));
});


gulp.task('default',['scripts', 'bower_js','bower_css','styles','scripts_back', 'styles_back' , 'bower_js_back', 'bower_css_back', 'watch']);

gulp.task('watch', function(){
	gulp.watch(PATHS_FRONT.SRC.JS,['scripts']);
	gulp.watch(PATHS_FRONT.SRC.CSS,['styles']);
	gulp.watch(PATHS_FRONT.SRC.BOWER_JS,['bower_js', 'bower_css']);

	gulp.watch(PATHS_BACK.SRC.JS,['scripts_back']);
	gulp.watch(PATHS_BACK.SRC.CSS,['styles_back']);
	gulp.watch(PATHS_BACK.SRC.BOWER_JS,['bower_js_back', 'bower_css_back']);
});
