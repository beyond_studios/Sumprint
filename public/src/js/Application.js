

var Application = function(){

  this.init = function(){
    this.parallax();
    this.homeSlider();



    $('#home-slider li').eq(0).addClass('active');
        $('#home-slider').bxSlider({
          infiniteLoop : true,
          controls : false,
          auto : true,
          responsive : true,
          onSliderLoad: function(){
              $(window).trigger('resize');

            },
            onSlideBefore: function(a, b){
              $('#home-slider li').removeClass('active');
              a.addClass('active');

            }
        });
        $('#menu-collapse').on('click', function(){
          $('.main-menu nav').toggleClass('active');
          return false;
        });
      };


  this.parallax = function(){

      $(window).on('scroll', function() {
          $('.roll').css('top', $(window).scrollTop() * -.09);
          $('.roll-init').css({
            'top': $(window).scrollTop() * .19 ,
            'opacity' : 1 - $(window).scrollTop() * .002
          });
      });
  };
  this.homeSlider = function(){
    $(window).on('resize', function() {
        $('.fixed-group').css('height', $('.main-banner').height());
    });

  }

};
var socket = io.connect('http://162.243.199.17:8367', { 'forceNew': true });

var Chat = function(){

  var t = this;
  this.CONNECTION = 90000;
  this.LOAD = false;

  socket.on('send_message_by_server_to_client', function(response){
    console.log(response);
    //receiveMessage
    t.receiveMessage(response.message);
  });

  this.register = function(){

  };
  this.close = function(){

  };

  this.connect  = function(){


  };


  //add events;
  this.events = function(){

    $('.form-chat-start input').on('input',function(){
      var f = true;
      $('.form-chat-start input').each(function(){
        var t = $(this);
        if(t.val().trim().length <= 0)
          f = false;
        else
          f = true;
      });

      if(f){
          $('#start-chat').removeAttr('disabled');
      }else{
        $('#start-chat').attr('disabled', true);
      }

    });
    if(clientToken){
      socket.emit('register_session_first',{ id_chat_session : clientToken} );
      socket.on('response_session_first', function(response){
        $.ajax({
          url : chat.session,
          type : 'POST',
          data: {id:response.socketId, sessionId:response.id_chat_session, _token:_token},
          success  : function(response){
            socket.emit('update_session',response );
          }
        });
      });
    }

    $('#init-chat').off('submit').on('submit', function(){
      var me = $(this);
      me.find('button').attr('disabled');

      $.ajax({
        url : chat.url,
        type : 'POST',
        data: me.serialize(),
        success  : function(response){
          socket.emit('register_session',{ id_chat_session : response} );
        }
      });

      return false;
    });

    socket.on('response_register_session',function(session){
      $.ajax({
        url : chat.session,
        type : 'POST',
        data: {id:session.socketId, sessionId:session.sessionId, _token:_token},
        success  : function(response){
          t.load();
        }
      });
    });



    $('#open-chat').off('submit').on('submit', function(){
      var me = $(this);
      var valueChat = $('.message_chat_box').val().trim();
      valueChat = valueChat.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/"/g, '&quot;');
      var ser = me.serialize();
      t.sendMessage(valueChat);
      $.ajax({
        url : chat.message,
        type : 'POST',
        data: ser,
        success  : function(response){
          socket.emit('send_message',{message:valueChat, id_chat_session : $('#id_chat_session').val()});
        }
      });
      return false;
    });



    $('#dismiss-chat').off('click').on('click', function(){
      var me = $(this);

      var contentDismissAlert = $('#dismiss-chat-cops');
      //show alert close dismiss
      contentDismissAlert.removeClass('hide');
      var cn = $('.chat-nemesis');
      cn.removeClass('minimized');
      localStorage.setItem('chat_minimized', 0);
      return false;
    });

    $('#real-close-chat').off('click').on('click', function(){
      var me=$(this);
      t.unload();
      me.attr('disabled', true);
      t.LOAD= false;
      $('.chat--prefab').removeAttr('disabled');
      localStorage.setItem('load_session_chat', 0);
      return false;
    });
    $('#nope-close-chat').off('click').on('click', function(){
      var contentDismissAlert = $('#dismiss-chat-cops');
      //hide alert close dismiss
      contentDismissAlert.addClass('hide');
      return false;
    });

    $('#minimized-chat').off('click').on('click', function(){
      var me = $(this);
      t.minimize();
      return false;
    });

    var min = (localStorage.getItem('chat_minimized'));

    if(min == 1) {
      $('.chat-nemesis').addClass('minimized');
    }
  };

  this.sendMessage = function(message){
    var data = '<div class="my message">';
        data+='<div class="wrap-message">';
        data+=message;
        data+='</div>';
        data+='</div>';
    var d =  $('#composer--chat')
    d.append(data);
    $('.message_chat_box').val('');
    d.scrollTop(d.prop("scrollHeight"));
  };
  this.receiveMessage = function(message){
    var data = '<div class="message">';
        data+='<div class="wrap-message">';
        data+=message;
        data+='</div>';
        data+='</div>';
    var d =  $('#composer--chat')
    d.append(data);
    $('.message_chat_box').val('');
    d.scrollTop(d.prop("scrollHeight"));
  };

  this.minimize = function(){
    var cn = $('.chat-nemesis');
    cn.toggleClass('minimized');

    if(cn.hasClass('minimized')){
      localStorage.setItem('chat_minimized', 1);
    }else{
      localStorage.setItem('chat_minimized', 0);
    }

  };
  this.load = function(){
    var g = $('#chat-composer');
    g.removeClass('hidden');
    g.html('<span class="loader-chat">Conectando al chat..</span>');
    $.ajax({
      url : chat.url,
      type : 'GET',
      success  : function(response){
          g.html(response);
          localStorage.setItem('load_session_chat', 1);
          t.events();
          t.connect();
          var d =  $('#composer--chat')
          d.scrollTop(d.prop("scrollHeight"));

      }
    });
  };
  this.unload = function(){
    $('#chat-composer').html('');
  };
  this.run = function(){
    // go

    $('.chat--prefab').off('click').on('click', function(){
      var me = $(this);
      t.load();
      me.attr('disabled', true);
      t.LOAD= true;
      return false;
    });
    if(localStorage.getItem('load_session_chat') == 1){
      $('.chat--prefab').trigger('click');
    }

  }

};

var Searcher = function(){
  var id = '#search-product';
  var current_tipo = [];
  var core = {
      init : function(){
        var contentPollSearcher =  $('.pool-search');

        $(id).off('input').on('input', function(){
          var t = $(this);
          var valueString = t.val();
          if(valueString.length>0){
            core.enable();
            contentPollSearcher.find('li>a').each(function(){
              var me = $(this);
              var getValue = me.text().trim();
              var expoReg =  new RegExp(".?"+(valueString.toLowerCase())+".?", 'g');
              var liElement = me.parent();
              if(!(getValue.toLowerCase()).match(expoReg)){
                if(!liElement.hasClass('hidden'))
                  liElement.addClass('hidden');
              }else{
                if(liElement.hasClass('hidden'))
                  liElement.removeClass('hidden')
              }
            });
          }else{

            core.disable();
          }
        });

        var determineInSidePool= true;

        contentPollSearcher .on('mousemove', function(){
          determineInSidePool = false;
        }).on('mouseleave', function(){
          determineInSidePool = true;
        });

        $(document).on('click', function(){
          if(determineInSidePool)
            core.disable()
        });

        contentPollSearcher.find('a').on('click', function(){

          //Construct element
          var me = $(this);
          var tipo = me.data('value');
          core.pushTipo(tipo);
          var data = '<li>';
              data +='<div class="cp-item">';
              data +='<span>'+ me.text() +'</span>';
              data +='<a href="'+tipo+'" class="close"></a>';
              data +='</div>';
              data +='</li>';

          me.closest('li').addClass('hidden-remove');
          core.bindSearch();

          $('.cp-list-append').append(data);
          $('#search-product').val('');

          //Delete added Item
          $('.cp-item').find('a.close').off('click').on('click', function(){
            var me = $(this);
            me.closest('li').remove();
            var tip = me.attr('href');
            contentPollSearcher.find('a[data-value='+tip+']').parent().removeClass('hidden-remove');
            var i = array.indexOf(tip);
            if(i != -1) {
              current_tipo.splice(i, 1);
            }
            $(id).trigger('input');
            core.bindSearch();
            return false;
          });
          return false;
        });
      },
    enable : function(){
      $('.capability').addClass('searching');
    },
    disable : function(){
      $('.capability').removeClass('searching');
    },
    pushTipo : function(item){
      if(typeof item[item] == 'undefined')
        current_tipo.push(item);
    },
    bindSearch : function(){
      $("#p-elements [data-tipo]").addClass('hidden');
      for(var key in current_tipo){
        console.log(current_tipo[key])
        var el = $("#p-elements [data-tipo='"+current_tipo[key]+"']");
        el.removeClass('hidden');
      }
    }
  };
  return core;
};
$(function(){


  var chat = new Chat();
  chat.run();

  var searcher = new Searcher();
  searcher.init();



});



