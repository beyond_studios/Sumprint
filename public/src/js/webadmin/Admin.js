
    var Admin = function(){
        var t = this;



        var core = {
            init : function(){
               $('.btn--edit').on('click', function(){
                   var t = $(this);
                    var form = $('form');
                   var content = t.closest('tr').find('.content-form');
                   var inputCollection = content.find('input');
                   inputCollection.each(function(){
                       var me = $(this);
                       var name = me.attr('name');

                       if(name.charAt(0)== '*'){
                           //iamge
                           var image= form.find('[data-type="'+ name+'"]');
                           var img = me.val();
                           var ext = img.substr(img.length-4, 1);
                           if(ext=="."){
                              image.attr('src',me.val());
                           }else{
                            image.hide();
                           }

                       }else if(name.charAt(0)== '+'){
                          var image= form.find('[data-type="'+ name+'"]');
                           image.attr('href',me.val());
                           image.html(me.val());
                       }else{
                           var input= form.find('[name="'+ name+'"]');

                           if((input.attr('type') == 'text')){
                               input.val(me.val());
                           }else if(input.is('textarea')){
                               var data =  me.val();
                               tinyMCE.get(input.attr('id')).setContent(data);
                           }else if(input.attr('type')=='checkbox'){
                              if(input.val()==me.val()){
                                input.prop('checked',true);
                              }else{
                                  input.prop('checked',false);
                              }
                           }
                       }

                   });
                   form.addClass('editing');
               });
               $('.btn--delete').on('click', function(){
                  var t = $(this);
                  var form = $('form');
                  var content = t.closest('tr').find('.content-form');
                  var tr = t.closest('tr');
                  var id = content.find('[name=id]').val();
                  var tipo = content.find('[name=tipo]').val();
                  request = $.ajax({
                      url: t.attr('href'),
                      type: "post",
                      data: {_token:_token,'id':id,'tipo':tipo}
                  });

                  // Callback handler that will be called on success
                  request.done(function (response, textStatus, jqXHR){
                      // Log a message to the console
                      tr.remove();
                  });
                  return false;
               });
                $('.btn--cancel').on('click', function(){
                    var t = $(this);
                    var form = $('form');
                    form.removeClass('editing');
                    form[0].reset();
                });

            }

        };
        return core;

    };


    $(function(){
        var admin = new Admin();
        admin.init();

        socket = io.connect('http://162.243.199.17:8367', { 'forceNew': true });
        socket.on('response_clients_active', function(response){
            var c = new ChatAdmin(response);
            c.tabChat();
            c.chat();
        });
        socket.on('response_session_first', function(response){
            console.log(response)
        });
        socket.emit('request_clients_active', '1');

        $( 'input[type=file]' ).on( 'change', function( e )
        {
            var t = $(this);
            var reader = new FileReader();
            reader.onload = function (e) {
                t.closest('.uploader-main').next().find('img')
                    .attr( 'src', e.target.result );
            };

            reader.readAsDataURL(this.files[0]);


        });

    });

