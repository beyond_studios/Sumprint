
    var ChatAdmin= function(response){

         t = this;
         threads = {};
         serverId = (response);
         objectSessionChat = function(){
            this.ajax = false;
            this.url = false;
            this.data = null;
            this.pending = true;
            this.idSession = -1;
        };

        var forInjectViewChatContent = '<div data-tab="{id_session}" data-session="{token_session}" class="contentMainChatActive"><img src="../images/loader.gif" alt=""></div>';
        var threadsSyncAcion = function(){
            var contentChat = $('#content-chat');

            for(var idSession in threads){
                var itemObjectChatSession = threads[idSession];

                if(!itemObjectChatSession.ajax){

                    itemObjectChatSession.idSession = idSession;

                    var replacedDataSession = forInjectViewChatContent
                        .replace('{id_session}', idSession)
                        .replace('{token_session}',itemObjectChatSession.tokenSession);
                    contentChat.append(replacedDataSession);

                    itemObjectChatSession.ajax = $.ajax({  type:'POST',data:{_token:_token,data:itemObjectChatSession},  url : itemObjectChatSession.url });
                    itemObjectChatSession.ajax.done(function(response){
                        itemObjectChatSession.pending = false;
                        itemObjectChatSession.data = response.view;
                        $('.contentMainChatActive[data-tab='+response.render.idSession+']').html(itemObjectChatSession.data);
                        core.chatSendMessage();

                    });
                }
            }

        };

        socket.on('update_session_to_server', function(response){
            $.ajax({
                url:chat_users,
                type:'POST',
                data: {_token:_token},
                success : function(users){
                    for(var key in response){
                        var tKey = (users[response[key]]);
                        $('[data-chat-session="'+tKey+'"]').attr('data-chat-token',response[key]);
                    }

                }
            })
        });

    var core = {

            chat : function(){

            socket.on('send_message_by_client', function(data) {
                var itemLayoutChat = $('#item-chat-'+data.id_chat_session);
                itemLayoutChat.attr('data-chat-token', data.id);

                if(itemLayoutChat.length>0){
                    //si existe la session chat
                    var badgeCountMessages =  itemLayoutChat.find('.item-count');
                    badgeCountMessages.show();
                    var badgeCountMessagesToInt = parseInt(badgeCountMessages.text());
                    badgeCountMessages.html(badgeCountMessagesToInt+1);
                }else{

                }
                var txtView= $('[data-tab='+data.id_chat_session+']').find('.bxChatController');
                core.rMessage(txtView, data.message);

            });

        },
        tabChat :function(){

            var tabChatContent = $('#tabchat');
            var linksTabChatContent = tabChatContent.find('a');
            linksTabChatContent.off('click').on('click', function(){


                var me = $(this);
                //remove badge on click
                me.find('.badge').fadeOut(300).html(0);
                linksTabChatContent.removeClass('active');
                me.addClass('active');
                var keyTempIdObject = me.data('chat-session');
                var contentChatTab = $('.contentMainChatActive');
                var contentChatTabActive = $('.contentMainChatActive[data-tab='+keyTempIdObject+']');
                contentChatTab.addClass('hidden');
                contentChatTabActive.removeClass('hidden');
                if(typeof threads[keyTempIdObject] == 'undefined') {
                    var createNewObjectChatSession = new objectSessionChat();

                    createNewObjectChatSession.url=chat_partial;
                    createNewObjectChatSession.ajax=false;
                    createNewObjectChatSession.pending=true;
                    createNewObjectChatSession.idSession=keyTempIdObject;
                    createNewObjectChatSession.tokenSession=me.attr('data-session');
                    createNewObjectChatSession.tokenClient=me.attr('data-chat-token');

                    //create a new thread
                    threads[keyTempIdObject] = createNewObjectChatSession;
                    threadsSyncAcion();
                }
                return false;
            });
        },
        chatSendMessage  : function(){
            var txtView = $('.bxChatController');
            txtView.off('keyup').on('keyup', function(e){
                var me = $(this);
                var tmpTokenSession = me.closest('.contentMainChatActive');
                var g = $('.messagesHistory').find('.contentWrapChat');
                g.scrollTop(g.prop("scrollHeight"));

                var dataIdSession = tmpTokenSession.attr('data-tab');
                var f = $('[data-chat-session='+ dataIdSession+']').attr('data-chat-token');
                if(e.keyCode == 13){
                    var msg = txtView.val().trim();
                    core.sMessage(txtView, msg);
                    core.sendMessageAdmin(msg, f, dataIdSession);
                    txtView.val('');
                }
            });
        },
        sMessage : function(txtView, msg){
            var g = txtView.closest('.messagesHistory').find('.contentWrapChat');
            g.append('<div class="messageBox text-right "><span>'+msg+'</span></div>');
            g.scrollTop(g.prop("scrollHeight"));
        },
        rMessage : function(txtView, msg){
            var g = txtView.closest('.messagesHistory').find('.contentWrapChat');
            g.append('<div class="messageBox text-left" ><span style="margin-left: 2px; background: #f0f98c">'+msg+'</span></div>');
            g.scrollTop(g.prop("scrollHeight"));

        },
        sendMessageAdmin : function(msg, clientToken, dataIdSession){

            $.ajax({
                url : chat_message,
                type : 'POST',
                data: {msg:msg, _token:_token, id:dataIdSession},
                success  : function(response){
                    socket.emit('send_message_by_server',{ message:msg, id:serverId, idClient:clientToken });
                }
            });
        }
     };

     return core;
    };

