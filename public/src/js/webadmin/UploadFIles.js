$(function() {
    var uploader = new Uploader({
        selectButton: "#uploader-button",
        url: "upload.php"
    });
    uploader
        .on("fileAdd", function() {
            console.log("fileAdd", arguments);
        })
        .on("fileRemove", function() {
            console.log("fileRemove", arguments);
        })
        .on("fileInvalid", function() {
            console.log("fileInvalid", arguments);
        })
        .on("tooManyFile", function() {
            console.log("tooManyFile", arguments);
        })
        .on("beforeUpload", function() {
            console.log("beforeUpload", arguments);
        })
        .on("uploadStart", function() {
            console.log("uploadStart", arguments);
        })
        .on("uploadProgress", function() {
            console.log("uploadProgress", arguments);
        })
        .on("uploadAbort", function() {
            console.log("uploadAbort", arguments);
        })
        .on("uploadComplete", function() {
            console.log("uploadComplete", arguments);
            alert(arguments[2]);
        })
        .on("uploadFail", function() {
            console.log("uploadFail", arguments);
        });
});