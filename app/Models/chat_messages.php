<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class chat_messages extends Model
{
    protected $table = 'chat_messages';

    protected $id = 'id';

    public $title = 'Chat Messages';

    protected $fillable = ['id_chat_session', 'mensaje', 'user_send', 'readed'];

    public $timestamps = false;
}
