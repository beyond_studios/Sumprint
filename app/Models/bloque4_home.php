<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class bloque4_home extends Model
{
    //
    protected $table = 'bloque4_home';

    protected $id = 'id';

    public $title = 'Bloque 4 del Home';

    protected $fillable = ['icono1', 'icono2', 'icono3', 'titulo', 'cabecera', 'imagen', 'link'];

    public $timestamps = false;
}
