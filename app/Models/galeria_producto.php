<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class galeria_producto extends Model
{
    //
    protected $table = 'galeria_producto';

    protected $id = 'id';

    public $title = 'Galería de Imagenes del Producto';

    protected $fillable = ['id', 'idproducto', 'imagen'];

    public $timestamps = false;
}
