<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class bloque3_home extends Model
{
    //
    protected $table = 'bloque3_home';

    protected $id = 'id';

    public $title = 'Bloque 3 del Home';

    protected $fillable = ['fondo', 'icono', 'titulo', 'cabecera'];

    public $timestamps = false;
}
