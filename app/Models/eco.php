<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class eco extends Model
{
    //
    protected $table = 'eco';

    protected $id = 'id';

    public $title = 'Eco';

    protected $fillable = ['titulo', 'descripcion'];

    public $timestamps = false;
}
