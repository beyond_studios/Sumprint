<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class contacto extends Model
{
    //
    protected $table = 'contacto';

    protected $id = 'id';

    public $title = 'Contacto';

    protected $fillable = ['telefono', 'correo', 'direccion'];

    public $timestamps = false;
}
