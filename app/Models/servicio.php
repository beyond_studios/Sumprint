<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class servicio extends Model
{
    //
    protected $table = 'servicio';

    protected $id = 'id';

    public $title = 'Servicios';

    protected $fillable = ['icono', 'titulo', 'descripcion', 'subtitulo1', 'descripcion1', 'subtitulo2', 'descripcion2', 'imagen', 'link'];

    public $timestamps = false;
}
