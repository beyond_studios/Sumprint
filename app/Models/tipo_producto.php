<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tipo_producto extends Model
{
    //
    protected $table = 'tipo_producto';

    protected $id = 'id';

    public $title = 'Tipo de Producto';

    protected $fillable = ['nombre'];

    public $timestamps = false;
}
