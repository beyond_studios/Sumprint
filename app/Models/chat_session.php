<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class chat_session extends Model
{
    protected $table = 'chat_session';

    protected $id = 'id';

    public $title = 'Chat Session';

    protected $fillable = ['nombre_usuario', 'dni_usuario', 'tel_usuario', 'ip_usuario','start_session_datetime', 'session'];

    public $timestamps = false;
}
