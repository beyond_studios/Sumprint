<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class promocion extends Model
{
    //
    protected $table = 'promocion';

    protected $id = 'id';

    public $title = 'Promocion';

    protected $fillable = ['titulo', 'cabecera', 'descripcion', 'detalle', 'imagen'];

    public $timestamps = false;
}
