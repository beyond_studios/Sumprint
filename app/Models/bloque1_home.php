<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class bloque1_home extends Model
{
    //
    protected $table = 'bloque1_home';

    protected $id = 'id';

    public $title = 'Bloque 1 del Home';

    protected $fillable = ['titulo', 'cabecera', 'descripcion', 'imagen', 'link'];

    public $timestamps = false;
}
