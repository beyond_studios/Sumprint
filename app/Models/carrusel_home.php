<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class carrusel_home extends Model
{
    //
    protected $table = 'carrusel_home';

    protected $id = 'id';

    public $title = 'Imágenes del Carrusel del Home';

    protected $fillable = ['titulo', 'cabecera', 'descripcion', 'imagen', 'link', 'icono', 'is_top'];

    public $timestamps = false;
}
