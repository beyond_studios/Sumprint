<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class bloque2_home extends Model
{
    //
    protected $table = 'bloque2_home';

    protected $id = 'id';

    public $title = 'Bloque 2 del Home';

    protected $fillable = ['titulo1', 'cabecera1', 'descripcion1', 'imagen1', 'titulo2', 'cabecera2', 'descripcion2', 'imagen2', 'link1', 'link2'];

    public $timestamps = false;
}
