<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class producto extends Model
{
    //
    protected $table = 'producto';

    protected $id = 'id';

    public $title = 'Producto';

    protected $fillable = ['id', 'idtipo', 'marca', 'nombre', 'imagen', 'activo', 'codigo', 'especificacion', 'top', 'descripcion', 'link_detalle', 'imagen2', 'imagen3', 'imagen4'];

    public $timestamps = false;
}
