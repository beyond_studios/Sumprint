<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class nosotros extends Model
{
    //
    protected $table = 'nosotros';

    protected $id = 'id';

    public $title = 'Nosotros';

    protected $fillable = ['quienessomos', 'mision', 'vision', 'imagen'];

    public $timestamps = false;
}
