<?php

namespace App\Http\Controllers;
use App\Models\chat_messages;
use App\Models\chat_session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use View;

class ChatController extends Controller
{

  public function box(){
      $session = (Session::get('chat_session', null));

      $messages = chat_messages::select('mensaje', 'user_send')
          ->where(['id_chat_session'=> Session::get('chat_session') ])->get();
    return View::make('site.chat.chat-box')->with('session', $session)->with('messages',$messages);
  }
  public function boxPost(){

      $input = Input::get('chat');

      $session = new chat_session;
      $session->nombre_usuario = $input['name'];
      $session->dni_usuario = $input['dni'];
      $session->tel_usuario = $input['phone'];
      $session->ip_usuario = $_SERVER['REMOTE_ADDR'];
      $session->save();
      Session::set('chat_session', $session->id);

      return $session->id;


  }
  public function chatSession(){
    $data = (Input::get());
      if(isset($data['sessionId'])){

      $session = chat_session::find($data['sessionId']);
      $session->session = $data['id'];
      $session->save();
      }

  }

  public function saveMessage(){
      if(Session::get('chat_session', null)){
          $message = new chat_messages();
          $message->id_chat_session =Session::get('chat_session');
          $message->mensaje = Input::get('send');
          $message->readed = 0;
          $message->save();
      }
  }

}
