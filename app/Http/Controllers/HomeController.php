<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\ContactFormRequest;
use App\Models\producto;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index()
    {
        return view('site.home.index');
    }
    public function about()
    {
        return view('site.about.index');
    }

    public function services()
    {
        return view('site.services.index');
    }
    public function promo()
    {
        return view('site.promo.index');
    }
    public function eco()
    {
        return view('site.eco.index');
    }
    public function products()
    {
        $productos = producto::paginate(12);
        return view('site.products.index')
            ->with('productos', $productos);
    }
    public function productDetail()
    {
        return view('site.products.detail');
    }
    public function productDetailId($id)
    {
        return view('site.products.detail')->with('id', $id);
    }
    public function contact()
    {
        return view('site.contact.index');
    }
    public function contactPost(ContactFormRequest $request){


        try{

        \Mail::send('emails.contact',
            array(
                'name' => $request->get('name'),
                'email' => $request->get('email'),
            ), function($message)
            {
                $message->from('no_reply__web@sumprint.com');
                $message->to('uaxp123@gmail.com', 'Admin')->subject('TODOParrot Feedback');
            });

        return \Redirect::route('contact')
            ->with('message', 'Tu mensaje ha sido enviado con éxito!');
        }catch(\Exception $e){
            return \Redirect::route('contact')
                ->with('message', $e->getMessage());
        }

    }
}
