<?php

namespace App\Http\Controllers\Admin;
use View;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;

use App\Models\contacto;
use App\Models\carrusel_home;
use App\Models\bloque1_home;
use App\Models\bloque2_home;
use App\Models\bloque3_home;
use App\Models\bloque4_home;
use App\Models\servicio;
use App\Models\eco;
use App\Models\promocion;
use App\Models\tipo_producto;
use App\Models\producto;
use App\Models\nosotros;

use Image; 

use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard(){

    return View::make('webadmin.dashboard');
  }

  public function contenidoMenu(){
  	return View::make('webadmin.contenidoMenu');
  }



  public function saveCarruselContenidoMenu(Request $request){
  	// Contenido de Carrusel
  	$datoCarrusel['titulo'] = $request->input('titulo_carrusel');
	$datoCarrusel['cabecera'] = $request->input('cabecera_carrusel');
	$datoCarrusel['descripcion'] = $request->input('descripcion_carrusel');
	$datoCarrusel['link'] = $request->input('link');
	if(Input::File('imagen_carrusel')){
		if(Input::File('imagen_carrusel')->isValid()){
			$destinoBloque1 = 'images/carrusel-home';
			$extension = Input::file('imagen_carrusel')->getClientOriginalExtension();
	      	$nombre = time().rand(11111,99999).'.'.$extension;
			Input::File('imagen_carrusel')->move($destinoBloque1, $nombre);
			$ruta = $destinoBloque1."/".$nombre;
			$img = Image::make($ruta);
			$img->resize(589, 613);
			$img->save($ruta);
			$datoCarrusel['imagen'] = $nombre;
		}
	}
    if(Input::get('id') != ''){
        $carrusel_home = carrusel_home::find(Input::get('id'));
        $carrusel_home->fill($datoCarrusel);
        $carrusel_home->save();
    }else{
        carrusel_home::create($datoCarrusel)->save();
    }
  	return View::make('webadmin.contenidoMenu');
  }

  public function saveContenidoMenu(Request $request){
  	$datoBloque1['titulo'] = $request->input('titulo_bloque1');
	$datoBloque1['cabecera'] = $request->input('cabecera_bloque1');
	$datoBloque1['descripcion'] = $request->input('descripcion_bloque1');
	$datoBloque1['link'] = $request->input('link_bloque1');
      $b = Input::File('imagen_bloque1', null);
      if($b) {
          if (Input::File('imagen_bloque1')->isValid()) {
              $destinoBloque1 = 'images/bloque1-home';
              $extension = Input::file('imagen_bloque1')->getClientOriginalExtension();
              $nombre = time().rand(11111, 99999) . '.' . $extension;
              Input::File('imagen_bloque1')->move($destinoBloque1, $nombre);
              $ruta = $destinoBloque1 . "/" . $nombre;
              $img = Image::make($ruta);
              $img->resize(722, 427);
              $img->save($ruta);
              $datoBloque1['imagen'] = $nombre;
          }
      }

  	bloque1_home::where([ 'id'=>$request->input('id_bloque1') ])->update($datoBloque1);
  	// Contenido de Bloque 2
  	$datoBloque2['titulo1'] = $request->input('titulo1_bloque2');
	$datoBloque2['cabecera1'] = $request->input('cabecera1_bloque2');
	$datoBloque2['descripcion1'] = $request->input('descripcion1_bloque2');
	$datoBloque2['link1'] = $request->input('link1_bloque2');
	$datoBloque2['link2'] = $request->input('link2_bloque2');

      $b1 = Input::File('imagen1_bloque2', null);
      if($b1){
            if(Input::File('imagen1_bloque2')->isValid()){
                $destinoBloque2 = 'images/bloque2-home';
                $extension = Input::file('imagen1_bloque2')->getClientOriginalExtension();
                $nombre = time().rand(11111,99999).'.'.$extension;
                Input::File('imagen1_bloque2')->move($destinoBloque2, $nombre);
                $ruta = $destinoBloque2."/".$nombre;
                $img = Image::make($ruta);
                $img->resize(385, 297);
                $img->save($ruta);
                $datoBloque1['imagen1'] = $nombre;
            }
      }

      $datoBloque2['titulo2'] = $request->input('titulo2_bloque2');
	$datoBloque2['cabecera2'] = $request->input('cabecera2_bloque2');
	$datoBloque2['descripcion2'] = $request->input('descripcion2_bloque2');

      $b2 = Input::File('imagen2_bloque2', null);
      if($b2) {
          if (Input::File('imagen2_bloque2')->isValid()) {
              $destinoBloque2 = 'images/bloque2-home';
              $extension = Input::file('imagen2_bloque2')->getClientOriginalExtension();
              $nombre = time().rand(11111, 99999) . '.' . $extension;
              Input::File('imagen2_bloque2')->move($destinoBloque2, $nombre);
              $ruta = $destinoBloque2 . "/" . $nombre;
              $img = Image::make($ruta);
              $img->resize(385, 297);
              $img->save($ruta);
              $datoBloque2['imagen2'] = $nombre;
          }
      }
  	bloque2_home::where([ 'id'=>$request->input('id_bloque2') ])->update($datoBloque2);
  	// Contenido de Bloque 3

      $b3 = Input::File('fondo_bloque3', null);
      if($b3) {
          if (Input::File('fondo_bloque3')->isValid()) {
              $destinoBloque3 = 'images/bloque3-home';
              $extension = Input::file('fondo_bloque3')->getClientOriginalExtension();
              $nombre = time().rand(11111, 99999) . '.' . $extension;
              Input::File('fondo_bloque3')->move($destinoBloque3, $nombre);
              $ruta = $destinoBloque3 . "/" . $nombre;
              $img = Image::make($ruta);
              $img->resize(1429, 1238);
              $img->save($ruta);
              $datoBloque3['fondo'] = $nombre;
          }
      }
      $b4 = Input::File('icono_bloque3', null);
      if($b4) {
          if (Input::File('icono_bloque3')->isValid()) {
              $destinoBloque3 = 'images/bloque3-home';
              $extension = Input::file('icono_bloque3')->getClientOriginalExtension();
              $nombre = time().rand(11111, 99999) . '.' . $extension;
              Input::File('icono_bloque3')->move($destinoBloque3, $nombre);
              $ruta = $destinoBloque3 . "/" . $nombre;
              $img = Image::make($ruta);
              $img->resize(73, 70);
              $img->save($ruta);
              $datoBloque3['icono'] = $nombre;
          }
      }
	$datoBloque3['titulo'] = $request->input('titulo_bloque3');
	$datoBloque3['cabecera'] = $request->input('cabecera_bloque3');
  	bloque3_home::where([ 'id'=>$request->input('id_bloque3') ])->update($datoBloque3);
  	// Contenido de Bloque 4
      $b5 = Input::File('icono1_bloque4', null);
      if($b5) {
          if (Input::File('icono1_bloque4')->isValid()) {
              $destinoBloque4 = 'images/bloque4-home';
              $extension = Input::file('icono1_bloque4')->getClientOriginalExtension();
              $nombre = time().rand(11111, 99999) . '.' . $extension;
              Input::File('icono1_bloque4')->move($destinoBloque4, $nombre);
              $ruta = $destinoBloque4 . "/" . $nombre;
              $img = Image::make($ruta);
              $img->resize(75, 70);
              $img->save($ruta);
              $datoBloque4['icono1'] = $nombre;
          }
      }
  	//$datoBloque4['icono1'] = $request->input('icono1_bloque4');
      $b6 = Input::File('icono2_bloque4', null);
      if($b6) {
          if (Input::File('icono2_bloque4')->isValid()) {
              $destinoBloque4 = 'images/bloque4-home';
              $extension = Input::file('icono2_bloque4')->getClientOriginalExtension();
              $nombre = time().rand(11111, 99999) . '.' . $extension;
              Input::File('icono2_bloque4')->move($destinoBloque4, $nombre);
              $ruta = $destinoBloque4 . "/" . $nombre;
              $img = Image::make($ruta);
              $img->resize(75, 70);
              $img->save($ruta);
              $datoBloque4['icono2'] = $nombre;
          }
      }
      $b7 = Input::File('icono3_bloque4', null);
      if($b7) {
          if (Input::File('icono3_bloque4')->isValid()) {
              $destinoBloque4 = 'images/bloque4-home';
              $extension = Input::file('icono3_bloque4')->getClientOriginalExtension();
              $nombre = time().rand(11111, 99999) . '.' . $extension;
              Input::File('icono3_bloque4')->move($destinoBloque4, $nombre);
              $ruta = $destinoBloque4 . "/" . $nombre;
              $img = Image::make($ruta);
              $img->resize(75, 70);
              $img->save($ruta);
              $datoBloque4['icono3'] = $nombre;
          }
      }
	$datoBloque4['titulo'] = $request->input('titulo_bloque4');
	$datoBloque4['cabecera'] = $request->input('cabecera_bloque4');
	$datoBloque4['link'] = $request->input('link_bloque4');

      $b8 = Input::File('imagen_bloque4', null);
      if($b8) {
          if (Input::File('imagen_bloque4')->isValid()) {
              $destinoBloque4 = 'images/bloque4-home';
              $extension = Input::file('imagen_bloque4')->getClientOriginalExtension();
              $nombre = time().rand(11111, 99999) . '.' . $extension;
              Input::File('imagen_bloque4')->move($destinoBloque4, $nombre);
              $ruta = $destinoBloque4 . "/" . $nombre;
              $img = Image::make($ruta);
              $img->resize(420, 410);
              $img->save($ruta);
              $datoBloque4['imagen'] = $nombre;
          }
      }
  	bloque4_home::where([ 'id'=>$request->input('id_bloque4') ])->update($datoBloque4);
  	return View::make('webadmin.contenidoMenu');
  }

  public function nosotros(){
  	return View::make('webadmin.nosotros');
  }

  public function saveNosotros(Request $request){
  	$datoNosotros['quienessomos'] = $request->input('quienessomos');
  	$datoNosotros['mision'] = $request->input('mision');
  	$datoNosotros['vision'] = $request->input('vision');
  	$imagen = Input::File('imagen', null);
  	if($imagen) {
      	if(Input::File('imagen')->isValid()){
          	$destinoServicio = 'images/nosotros';
          	$extension = Input::file('imagen')->getClientOriginalExtension();
          	$nombre = time().rand(11111,99999).'.'.$extension;
          	Input::File('imagen')->move($destinoServicio, $nombre);
			$ruta = $destinoServicio."/".$nombre;
			$img = Image::make($ruta);
			$img->resize(1170, 458);
			$img->save($ruta);
          	$datoNosotros['imagen'] = $nombre;
      	}
  	}
  	nosotros::where([ 'id'=>$request->input('id') ])->update($datoNosotros);
  	return View::make('webadmin.nosotros');
  }

  public function servicio(){
  	return View::make('webadmin.servicio');
  }

  public function saveServicio(Request $request){
  	$datoServicio['titulo'] = $request->input('titulo');
  	$datoServicio['descripcion'] = $request->input('descripcion');
  	$datoServicio['subtitulo1'] = $request->input('subtitulo1');
  	$datoServicio['descripcion1'] = $request->input('descripcion1');
  	$datoServicio['subtitulo2'] = $request->input('subtitulo2');
  	$datoServicio['descripcion2'] = $request->input('descripcion2');
  	$datoServicio['link'] = $request->input('link');


 	 $icono = Input::File('icono', null);
  	if($icono){
        if(Input::File('icono')->isValid()){
            $destinoServicio = 'images/servicios/icono';
            $extension = Input::file('icono')->getClientOriginalExtension();
            $nombre = time().rand(11111,99999).'.'.$extension;
            Input::File('icono')->move($destinoServicio, $nombre);
			$ruta = $destinoServicio."/".$nombre;
			$img = Image::make($ruta);
			$img->resize(75, 70);
			$img->save($ruta);
            $datoServicio['icono'] = $nombre;
        }
  	}

  	$imagen = Input::File('imagen', null);
  	if($imagen) {
      	if(Input::File('imagen')->isValid()){
          	$destinoServicio = 'images/servicios/imagen';
          	$extension = Input::file('imagen')->getClientOriginalExtension();
          	$nombre = time().rand(11111,99999).'.'.$extension;
          	Input::File('imagen')->move($destinoServicio, $nombre);
			$ruta = $destinoServicio."/".$nombre;
			$img = Image::make($ruta);
			$img->resize(700, 460);
			$img->save($ruta);
          	$datoServicio['imagen'] = $nombre;
      	}
  	}

    if(Input::get('id') != ''){
        $servicio = servicio::find(Input::get('id'));
        $servicio->fill($datoServicio);
        $servicio->save();
    }
  	return  Redirect::route('webadmin.servicio');

  }


  public function eco(){
  	return View::make('webadmin.eco');
  }

  public function saveEco(Request $request){
  	$datoEco['titulo'] = $request->input('titulo');
  	$datoEco['descripcion'] = $request->input('descripcion');
  	eco::where(['id'=>$request->input('id')])->update($datoEco);
  	return View::make('webadmin.eco');
  }

  public function promocion(){
  	return View::make('webadmin.promocion');
  }

  public function savePromocion(Request $request){
  	$datoPromocion['titulo'] = $request->input('titulo');
  	$datoPromocion['cabecera'] = $request->input('cabecera');
  	$datoPromocion['descripcion'] = $request->input('descripcion');
  	$datoPromocion['detalle'] = $request->input('detalle');
  	if(Input::File('imagen', null)){
	  	if(Input::File('imagen')->isValid()){
			$destinoPromocion = 'images/promociones';
			$extension = Input::file('imagen')->getClientOriginalExtension();
	      	$nombre = time().rand(11111,99999).'.'.$extension;
			Input::File('imagen')->move($destinoPromocion, $nombre);
			$ruta = $destinoPromocion."/".$nombre;
			$img = Image::make($ruta);
			$img->resize(1172, 571);
			$img->save($ruta);
			$datoPromocion['imagen'] = $nombre;
		}
	}

    if(Input::get('id') != ''){
        $promocion = promocion::find(Input::get('id'));
        $promocion->fill($datoPromocion);
        $promocion->save();
    }else{
        promocion::create($datoPromocion)->save();
    }
  	return View::make('webadmin.promocion');
  }

  public function producto(){
  	return View::make('webadmin.producto');
  }

  public function saveProducto(Request $request){
  	$datoProducto['idtipo'] = $request->input('idtipo');
  	$datoProducto['marca'] = $request->input('marca');
  	$datoProducto['nombre'] = $request->input('nombre');
  	if(Input::File('imagen')){
	  	if(Input::File('imagen')->isValid()){
			$destinoProducto = 'images/productos';
			$extension = Input::file('imagen')->getClientOriginalExtension();
	      	$nombre = time().rand(11111,99999).'.'.$extension;
			Input::File('imagen')->move($destinoProducto, $nombre);
			$ruta = $destinoProducto."/".$nombre;
			$img = Image::make($ruta);
			$img->resize(323, 353);
			$img->save($ruta);
			$rutaTh = $destinoProducto."/th_".$nombre;
			$img->resize(70,91);
			$img->save($rutaTh);
			$datoProducto['imagen'] = $nombre;
		}
	}
  	if(Input::File('imagen2')){
	  	if(Input::File('imagen2')->isValid()){
			$destinoProducto = 'images/productos';
			$extension = Input::file('imagen2')->getClientOriginalExtension();
	      	$nombre = time().rand(11111,99999).'.'.$extension;
			Input::File('imagen2')->move($destinoProducto, $nombre);
			$ruta = $destinoProducto."/".$nombre;
			$img = Image::make($ruta);
			$img->resize(323, 353);
			$img->save($ruta);
			$rutaTh = $destinoProducto."/th_".$nombre;
			$img->resize(70,91);
			$img->save($rutaTh);
			$datoProducto['imagen2'] = $nombre;
		}
	}
  	if(Input::File('imagen3')){
	  	if(Input::File('imagen3')->isValid()){
			$destinoProducto = 'images/productos';
			$extension = Input::file('imagen3')->getClientOriginalExtension();
	      	$nombre = time().rand(11111,99999).'.'.$extension;
			Input::File('imagen3')->move($destinoProducto, $nombre);
			$ruta = $destinoProducto."/".$nombre;
			$img = Image::make($ruta);
			$img->resize(323, 353);
			$img->save($ruta);
			$rutaTh = $destinoProducto."/th_".$nombre;
			$img->resize(70,91);
			$img->save($rutaTh);
			$datoProducto['imagen3'] = $nombre;
		}
	}
  	if(Input::File('imagen4')){
	  	if(Input::File('imagen4')->isValid()){
			$destinoProducto = 'images/productos';
			$extension = Input::file('imagen4')->getClientOriginalExtension();
	      	$nombre = time().rand(11111,99999).'.'.$extension;
			Input::File('imagen4')->move($destinoProducto, $nombre);
			$ruta = $destinoProducto."/".$nombre;
			$img = Image::make($ruta);
			$img->resize(323, 353);
			$img->save($ruta);
			$rutaTh = $destinoProducto."/th_".$nombre;
			$img->resize(70,91);
			$img->save($rutaTh);
			$datoProducto['imagen4'] = $nombre;
		}
	}
	$datoProducto['codigo'] = $request->input('codigo');
	$datoProducto['especificacion'] = $request->input('especificacion');
	$datoProducto['top'] = $request->input('top');
	$datoProducto['descripcion'] = $request->input('descripcion');
  	if(Input::File('link_detalle')){
	  	if(Input::File('link_detalle')->isValid()){
			$destinoProducto = 'images/productos/detalle';
			$extension = Input::file('link_detalle')->getClientOriginalExtension();
	      	$nombre = time().rand(11111,99999).'.'.$extension;
			Input::File('link_detalle')->move($destinoProducto, $nombre);
			$ruta = $destinoProducto."/".$nombre;
			$datoProducto['link_detalle'] = $nombre;
		}
	}

    if(Input::get('id') != ''){
        $producto = producto::find(Input::get('id'));
        $producto->fill($datoProducto);
        $producto->save();
    }else{
        producto::create($datoProducto)->save();
    }
  	return View::make('webadmin.producto');
  }

  public function tipoProducto(){
  	return View::make('webadmin.tipoProducto');
  }

  public function saveTipoProducto(Request $request){
  	$datoTipoProducto['nombre'] = $request->input('nombre');
    if(Input::get('id') != ''){
        $tipoproducto = tipo_producto::find(Input::get('id'));
        $tipoproducto->fill($datoTipoProducto);
        $tipoproducto->save();
    }else{
        tipo_producto::create($datoTipoProducto)->save();
    }
  	return View::make('webadmin.tipoProducto');
  }

  public function datoContacto(){
  	return View::make('webadmin.datoContacto');
  }

  public function saveContacto(Request $request){
  	$dataContacto['telefono'] = $request->input('telefono');
  	$dataContacto['correo'] = $request->input('correo');
  	$dataContacto['direccion'] = $request->input('direccion');
  	contacto::where([ 'id'=>$request->input('id') ])->update($dataContacto);

  	return View::make('webadmin.datoContacto');
  }

  public function delete(){
  	$id = Input::get('id');
  	$tipo = Input::get('tipo');
  	if($tipo=="TP"){
  		tipo_producto::find($id)->delete();
  	}elseif($tipo=="SV"){
  		servicio::find($id)->delete();
  	}elseif($tipo=="PM"){
  		promocion::find($id)->delete();
  	}elseif($tipo=="PR"){
  		producto::find($id)->delete();
  	}elseif($tipo=="CR"){
  		carrusel_home::find($id)->delete();
  	}
  }

}
