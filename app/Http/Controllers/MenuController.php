<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class MenuController extends Controller
{
    //
    public function index(){
    	$items = [
    		'home'		  	=> [],
    		'about'		  	=> [],
    		'contact-us'	=> [],
    		'login'			=> [],
    		'register'		=> []
    	];
    	return view('webadmin.index', compact('items'));

    }
}
