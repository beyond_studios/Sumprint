<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/nosotros', 'HomeController@about')->name('about');
Route::get('/servicios', 'HomeController@services')->name('services');
Route::get('/productos', 'HomeController@products')->name('products');
Route::get('/productos-detalle/', 'HomeController@productDetail')->name('product.detail');
Route::get('/productos-detalle-id/{id}', 'HomeController@productDetailId')->name('product.detail.id');
Route::get('/promociones', 'HomeController@promo')->name('promo');
Route::get('/eco', 'HomeController@eco')->name('eco');
Route::get('/contacto', 'HomeController@contact')->name('contact');
Route::post('/contacto-send', 'HomeController@contactPost')->name('contact.form');

Route::get('/retrieve-chat', 'ChatController@box')->name('get.chat');
Route::post('/retrieve-chat', 'ChatController@boxPost')->name('get.chat.post');
Route::post('/start-session','ChatController@chatSession')->name('chat.session');
Route::post('/send-message','ChatController@saveMessage')->name('post.message');



    Route::group(['prefix' => 'webadmin'], function()
    {
        Route::get('/','Admin\AdminController@dashboard');

        Route::get('/chat','Admin\AdminController@chatAction')->name('webadmin.chat');
        Route::post('/chat-render','Admin\AdminController@chatRender')->name('webadmin.chat.render');
        Route::post('/chat-send-message','Admin\AdminController@postMessage')->name('webadmin.post.message');
        Route::post('/get-chat-users','Admin\AdminController@getSessionUsers')->name('webadmin.chat.users');

        Route::post('/saveCarruselContenidoMenu','Admin\AdminController@saveCarruselContenidoMenu')->name('webadmin.saveCarruselContenidoMenu');
        Route::get('/contenidoMenu','Admin\AdminController@contenidoMenu')->name('webadmin.contenidoMenu');
        Route::post('/saveContenidoMenu','Admin\AdminController@saveContenidoMenu')->name('webadmin.saveContenidoMenu');

        Route::get('/servicio','Admin\AdminController@servicio')->name('webadmin.servicio');
		Route::post('/saveServicio', 'Admin\AdminController@saveServicio')->name('webadmin.saveServicio');
		Route::get('/eco','Admin\AdminController@eco')->name('webadmin.eco');
		Route::post('/saveEco', 'Admin\AdminController@saveEco')->name('webadmin.saveEco');
        Route::get('/nosotros','Admin\AdminController@nosotros')->name('webadmin.nosotros');
        Route::post('/saveNosotros', 'Admin\AdminController@saveNosotros')->name('webadmin.saveNosotros');
		Route::get('/promocion','Admin\AdminController@promocion')->name('webadmin.promocion');
		Route::post('/savePromocion', 'Admin\AdminController@savePromocion')->name('webadmin.savePromocion');
		Route::get('/producto','Admin\AdminController@producto')->name('webadmin.producto');
		Route::post('/saveProducto','Admin\AdminController@saveProducto')->name('webadmin.saveProducto');
		Route::get('/tipoProducto','Admin\AdminController@tipoProducto')->name('webadmin.tipoProducto');
		Route::post('/saveTipoProducto','Admin\AdminController@saveTipoProducto')->name('webadmin.saveTipoProducto');
		Route::get('/datoContacto','Admin\AdminController@datoContacto')->name('webadmin.datoContacto');
		Route::post('/saveContacto','Admin\AdminController@saveContacto')->name('webadmin.saveContacto');

        Route::get('/login', ['as' => 'loginRoute', 'uses' =>'Auth\AuthController@getLogin']);
        Route::post('/login', 'Auth\AuthController@postLogin');
        Route::get('/logout', ['as' => 'logout', 'uses' =>'Auth\AuthController@getLogout']);

        Route::post('/delete','Admin\AdminController@delete')->name('webadmin.delete');


    });



Route::get('/home', 'HomeController@index');


